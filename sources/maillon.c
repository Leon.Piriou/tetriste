#include "maillon.h"


/// @brief Creation de la place memoire pour un maillon contenant une piece
/// @param p Pointeur sur la piece a mettre dans le maillon 
/// @note complexite : constant
/// @return NULL en cas d'echec de l'allocation ; le pointeur du maillon sinon
Maillon *creer_maillon(Piece *p){
    Maillon *m = malloc(sizeof(Maillon));
    if(!(m)) return NULL;

    m->piece = p;
    m->prec = NULL;
    m->suiv = NULL;

    return m;
}
