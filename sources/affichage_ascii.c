#include "affichage_ascii.h"

static void affichage_efface(void);
static void affichage_bandeau(void);

// ==================FONCTIONS STATIQUES================================================
/// @brief Efface l'ecran
 static void affichage_efface(){
    printf("\033[H");
    printf("\033[2J");
}

/// @brief Affiche 'Tetriste v1.0' en haut a gauche en couleur
static void affichage_bandeau(){
    printf("\033[31mT\033[32mE\033[33mT\033[34mR\033[35mI\033[36mS\033[33mT\033[32mE\033[39m v1.0\n\n");
}
// =============================================================================


/// @brief Affiche le nom du jeu en ascii art
int term_aff_bienvenue(void *nul){
    
    #ifndef DEBUG_TETRISTE
        affichage_efface();
    #endif
    affichage_bandeau();
    printf(
            "\n\t+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n"
            "\t|\t\t\t     \033[31m _____  \033[32m         \033[33m _       \033[34m        \033[35m _     \033[36m        \033[33m _      \033[32m\033[39m         \t\t\t\t\t|     \n"
            "\t|\t\t\t     \033[31m|_   _| \033[32m  ___    \033[33m| |_     \033[34m _ _    \033[35m(_)    \033[36m ___    \033[33m| |_    \033[32m ___ \033[39m \t\t\t\t\t|\n"
            "\t|\t\t\t     \033[31m  | |   \033[32m / -_)   \033[33m|  _|    \033[34m| '_|   \033[35m| |    \033[36m(_-<    \033[33m|  _|   \033[32m/ -_)\033[39m \t\t\t\t\t|\n"
            "\t|\t\t\t     \033[31m _|_|_  \033[32m \\___|   \033[33m_\\__|   _\033[34m|_|_   _\033[35m|_|_   \033[36m/__/_   \033[33m_\\__|   \033[32m\\___|\033[39m \t\t\t\t\t|\n"
            "\t|\t\t\t    _|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|\t\t\t\t\t|\n"
            "\t|\t\t\t    \"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0- \t\t\t\t\t|\n"
            "\t|                                                                       \t\t\t\t\t\t\t|\n"
            "\t|                                                                       \t\t\t\t\t\t\t|\n"
            "\t+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+\n"
            "\t\033[3;1mversion:1.0                                       \t\t\t\t\t\t\033[0mPar:\033[3;4mVincent Fablet\033[0m et \033[4;3mLeon Piriou\033[0m\n"
            "\n\n\n\n"
        );
    return 1;
}


/// @brief Affiche les regles du jeu
int term_aff_regles(void *nul){
    #ifndef DEBUG_TETRISTE
        affichage_efface();
    #endif
    affichage_bandeau();
    printf(
        "\t\033[4mREGLES DU JEU :\033[0m\n"
        "\tLe but est de reussir a aligner cote a cote\n"
        "\tplus de trois pieces de meme couleur ou de meme forme pour gagner des points.\n"
        "\tPour cela il faut placer les nouvelles pieces une a une\n"
        "\tsoit a droite soit a gauche de celles deja presente.\n\n"

        "\tOn peut egalement faire des rotations du plateau de jeu\n"
        "\tvers la gauche avant de poser une piece,\n" 
        "\tmais uniquement par forme ou par couleur.\n"
        "\tAinsi si on a le plateau suivant : \n"
        "\t\t\033[31mC\033[33mL\033[32mC\033[34mT\033[32mR\033[32mL\033[0m\n"
        "\tEt que l'on fait une rotation (vers la gauche) des pieces vertes, on obtiendra :\n"
        "\t\t\033[31mC\033[33mL\033[32mR\033[34mT\033[32mL\033[32mC\033[0m\t(On voit que la piece la plus a gauche est passe de l'autre cote)\n\n"
        
        "\tLe jeu s'arrete lorsqu'il y a plus de 15 pieces sur le terrain, ou si l'on quitte la partie.\n\n"

        "\t\033[4mCOMMANDES :\033[0m\n"
        "\tDans l'invite de commande, on utilisera :\n"
        "\t\t- 'e' Pour afficher ces explications\n"
        "\t\t- 'q' Pour quitter la partie (en la sauvegardant)\n"
        "\t\t- 'g' Pour placer la piece a gauche du terrain\n"
        "\t\t- 'd' Pour placer la piece a droite du terrain\n"
        "\t\t- 'r' Pour faire une rotation selon les types suivants :\n"
        "\t\t\t* 'R' : rouge\n"
        "\t\t\t* 'V' : vert\n"
        "\t\t\t* 'J' : jaune\n"
        "\t\t\t* 'B' : bleu\n"
        "\t\t\t* 'L' : losange\n"
        "\t\t\t* 'C' : carre\n"
        "\t\t\t* 'T' : triangle\n"
        "\t\t\t* 'O' : rond\n"
        "\n\n"
    );
    if(term_question("'o' ou 'n' pour jouer", NULL)){
        return 1;
    }
    else{
        return 0;
    }
    return 1;
}


/// @brief Recupere le pseudo de l'utilsateur et verifie qu'il est bien alphanumerique.
/// @param pseudo Pointeur sur l'espace servant a recuperer le pseudo (necessaire: MAX_PSEUDO char) 
int term_recup_pseudo(char *pseudo, void *nul){
    int valide = 1;
    do{
        valide = 1;
        int i = 0;
        printf("\033[0JVotre pseudo (max %d car.) : ", MAX_PSEUDO-1);
        fgets(pseudo, MAX_PSEUDO, stdin);
        if(pseudo[strlen(pseudo)-1] == '\n'){
            pseudo[strlen(pseudo)-1] = '\0';
        }
        if(strlen(pseudo) == 0){
            valide = 0;
        }
        while (pseudo[i] != '\0'){
            if(!isalnum(pseudo[i])){
                valide = 0;
            }
            i++;
        }
    if(!valide) printf("Pseudo invalide !\n");
    }while(!valide);
    return 1;
}

/// @brief Pose une question dont la reponse peut etre oui ou non
/// @param char_question Question a afficher
/// @param nul laisser NULL car inutile
/// @note complexite constante
/// @return 0 si la reponse de l'utilisateur est non, 1 sinon
int term_question(char *char_question, void *nul){
    char c;
    do{
        printf("%s (o/n) :",char_question);
        scanf("%c", &c);
        while(getchar() != '\n');
        if(c == 'o'){
            return 1;
        }
        else if(c == 'n'){
            return 0;
        }
    }while(1);
}

/// @brief Affiche la partie apres chaque coup du joueur
/// @param J le jeu a afficher
/// @param nul laisser NULL car inutile
/// @note complexite constante
/// @return 1 dans tous les cas
int term_aff_partie(Jeu *J, void *nul){
    #ifndef DEBUG_TETRISTE
        affichage_efface();
    #endif
    affichage_bandeau();
    printf("\tScore : %d\n", J->score);
    printf("\tProchaines pieces : ...");
    liste_affiche(J->prochaines_pce);
    printf("\n\tRecommandation : (a venir)\n\n");
    printf("\t __________\n");
    printf("\t|\n\t  ");
    liste_affiche(J->terr->liste[GENERALE]);
    printf("\t\\__________\n\n\n");
    return 1;
}

/// @brief Pose une question dont la reponse peut etre oui ou non
/// @param cmd recupere la commande
/// @param param recupere le type de rotation en cas de rotation
/// @param nul laisser NULL car inutile
/// @note complexite constante
/// @return 0 en cas de mauvaise commande, 1 sinon
int term_recup_cmd(int *cmd, int *param, void *nul){
    char types_cmd[NBRE_CMDS] = {'q', 'd', 'g', 'r', 'e'};
    char entree = 0;
    *cmd = -1 ; *param = 0;

    printf("commande :");
    int r = scanf("%c", &entree);
    while(getchar() != '\n');   // vide stdin
    #ifdef DEBUG_AFFICHAGE_ASCII
    printf("-- %s : entree:%c\n", __func__, entree);
    #endif
    if(r == 1){
        for(int i=0 ; i<NBRE_CMDS ; i++){
            if(entree == types_cmd[i]){
                *cmd = i;
                break;
            }
        }
        #ifdef DEBUG_AFFICHAGE_ASCII
            printf("-- %s : entree:%c\tcmd:%d\n", __func__, entree, *cmd);
        #endif
        if(*cmd == 3){
            char types_rot[NBRE_COUL_FORM-1] = {'R', 'V', 'J', 'B', 'L', 'C', 'T', 'O'};
            printf("\033[1F\033[0Jtype de rotation :");
            r = scanf("%c", &entree);
            while(getchar() != '\n');
            if(r == 1 && entree != '\n'){
                for(int i=0 ; i<NBRE_COUL_FORM-1 ; i++){
                    if(entree == types_rot[i]){
                        *param = i+1;
                        break;
                    }
                }
            }
            if(!(*param)){
                *cmd = -1;
            }
        }
    }
    if(*cmd == -1){
        return 0;
    }
    return 1;
}
