#include "joueur.h"

static int joueurs_cmp(const void *j1, const void *j2);


// =============== FONCTIONS STATIQUES ==================================================================================================

/// @brief Fonction de comparaison pour tri qsort dans l'ordre decroissant dans 'joueur_sauvegarde_classement'
/// @param j1 Joueur1
/// @param j2 Joueur2
/// @note complexite : constant
/// @return un nombre neg si j1 a plus grand score que j2, pos si le contraire, 0 si egaux
static int joueurs_cmp(const void *j1, const void *j2){
    #ifdef DEBUG_JOUEUR
        printf("-- %s : j1:%d \tj2%d \tres calc:%d\n", __func__, 
        (*(Joueur **) j1)->score, (*(Joueur **) j2)->score, (*(Joueur **) j1)->score - (*(Joueur**) j2)->score);
    #endif
    return (*(Joueur **) j2)->score - (*(Joueur**) j1)->score;
}

// =======================================================================================================================================



/// @brief Creation d'un joueur avec un score et un pseudo
/// @param pseudo chaine de car. de taille MAX_PSEUDO copiee dans un nouvel espace memoire
/// @param score score du joueur
/// @note complexite : constant
/// @return un pointeur sur un joueur ou NULL en cas d'echec
Joueur *creer_joueur(char *pseudo, int score){
    Joueur *j = malloc(sizeof(Joueur));
    if(j == NULL) return NULL;
    
    strncpy(j->pseudo, pseudo, MAX_PSEUDO);
    j->score = score;
    return j;
}

/// @brief Liberation des joueurs de tab entre l'indice 0 et n. Liberation de tab ensuite
/// @param tab tableau a liberer
/// @note complexite : lineaire (selon argument n)
/// @param n indice du plus grand element a libere
void libere_tab_joueur(Joueur **tab, int n){
    for(int i=0; i<n ; i++){
        free(tab[i]);
    }
    free(tab);
}


/// @brief Ouvre 'F_CLASSEMENT'. Charge les joueurs dans un tableau. Verifie si le 'pseudo' est dans le fichier. Si non, l'ajoute a la fin.
/// @param pseudo Pseudo du joueur de la partie dont on souhaite savoir si il est dans 'F_CLASSEMENT'
/// @param n_joueurs recupere a la fin de la fonction le nombre de joueur dans le tableau renvoye
/// @param place recupere l'indice dans le tableau de la struct joueur associe au pseudo.
/// @param est_nveau a la fin de la fonction, vaut 1 si le pseudo n'est pas present dans le classement ; 0 si il y est.
/// @note complexite : lineaire (selon n_joueurs)
/// @return NULL en cas d'echec (les allocations alors faites sont toutes liberees) ; Un tableau de 'Joueur' sinon
Joueur **charge_classement(char *pseudo, int *n_joueurs, int *place, int *est_nveau){
    int r;
    *place = -1;
    *n_joueurs = 0;

    FILE *f = fopen(F_CLASSEMENT, "r"); // on suppose F_CLASSEMENT deja triee
    if(f == NULL){
        #ifdef DEBUG_JOUEUR
            printf("-- %s : Erreur ouverture '%s'\n", __func__, F_CLASSEMENT);
        #endif
        return NULL;
    }

    r = fscanf(f, "TETRISTE CLASSEMENT:\nNombre d'inscris:%d\n", n_joueurs);
    if(r != 1){
        #ifdef DEBUG_JOUEUR
            printf("-- %s : Erreur recuperation nbre joueurs dans '%s'\n", __func__, F_CLASSEMENT);
        #endif
        fclose(f);
        return NULL;
    }

    Joueur **tab_j = malloc(sizeof(Joueur*)*(*n_joueurs));
    if(tab_j == NULL){
        #ifdef DEBUG_JOUEUR
            printf("-- %s : Erreur creation tableau de joueurs\n", __func__);
        #endif
        fclose(f);
        return NULL;
    }
    int s = -1;
    char p[MAX_PSEUDO] = {0};
    for(int i=0 ; i<(*n_joueurs) ; i++){
        r = fscanf(f, "%s : %d\n", p, &s);
        if(r != 2 || s == -1){
            #ifdef DEBUG_JOUEUR
                printf("-- %s : Erreur lecture du '%s'\n", __func__, F_CLASSEMENT);
            #endif
            libere_tab_joueur(tab_j, i);
            fclose(f);
            return NULL;
        }
        tab_j[i] = creer_joueur(p,s);
        if(strcmp(p, pseudo) == 0){
            // normalement, les pseudos sont uniques
            if(*place != -1){
                printf("-- %s : Attention, il y a plusieurs fois le meme pseudo dans '%s'\n", __func__, F_CLASSEMENT);
            }
            *est_nveau = 0;
            *place = i;
        }
    }
    fclose(f);

    // on ajoute le nouveau joueur a la fin si il n'est pas deja present :
    if(*place == -1){
        tab_j = realloc(tab_j, sizeof(Joueur*) * (*n_joueurs + 1));
        if(!(tab_j && (tab_j[*n_joueurs] = creer_joueur(pseudo, 0)))){
            printf("-- %s : Erreur lors de l'ajout final du nouveau joueur\n", __func__);
            libere_tab_joueur(tab_j, *n_joueurs);
            return NULL;
        }
        *est_nveau = 1;
        *place = *n_joueurs;
        (*n_joueurs)++;
    }
    #ifdef DEBUG_JOUEUR
        for(int i=0 ; i<*n_joueurs ; i++){
            printf("-- %s : tab_j[%d] = ps:%s sc:%d\n", __func__, i, tab_j[i]->pseudo, tab_j[i]->score);
        }
    #endif
    return tab_j;
}


/// @brief Trie dans l'ordre decroissant les joueurs selon leur score et le stocke dans le fichier F_CLASSEMENT
/// @param tab_j Tableau des joueurs
/// @param n_joueurs taille du tableau
/// @note complexite : n * log(n) avec n = n_joueurs
/// @return 0 si echec ; 1 si succes
int joueur_sauvegarde_classement(Joueur **tab_j, int n_joueurs){
    // mise a jour classement
    qsort(tab_j, n_joueurs, sizeof(Joueur*), joueurs_cmp);
    #ifdef DEBUG_JOUEUR
        printf("-- %s :tableau joueurs triee :\n", __func__);
        for(int i=0 ; i<n_joueurs ; i++){
            printf("%s %d\n", tab_j[i]->pseudo, tab_j[i]->score);
        }
    #endif

    // Ecriture du classement.
    FILE *f = fopen(F_CLASSEMENT, "w");
    if(f == NULL){
        printf("-- %s : Erreur d'ouverture de %s pour ecriture", __func__, F_CLASSEMENT);
        return 0;
    }
    fprintf(f, "TETRISTE CLASSEMENT:\nNombre d'inscris:%d\n", n_joueurs);
    for(int i=0 ; i<n_joueurs ; i++){
        fprintf(f, "%s : %d\n", tab_j[i]->pseudo, tab_j[i]->score);
    }
    fclose(f);
    return 1;
}

/// @brief Recupere les 10 premiers du fichier de classement (F_CLASSEMENT)
/// @note complexite : lineaire (selon le nombre de joueur dans les meilleurs)
/// @return 0 en cas d'echec ; 1 succes
int extraction_dix_premier()
{
    FILE *f = fopen(F_CLASSEMENT, "r");
    FILE *f2 = fopen(F_PREMIERS,"w");
    if(f2==NULL)
    {
        printf("erreur le fichier n'a pas reussi a etre ouvert\n"); 
        return 0;
    }

    if(f==NULL)
    {
        printf("erreur le fichier n'a pas reussi a etre ouvert\n"); 
        fclose(f2);
        return 0;
    }
    int nb;
    int verif=fscanf(f,"TETRISTE CLASSEMENT:\nNombre d'inscris:%d\n",&nb);
    if(verif!=1)
    {
        printf("erreur le nombre dinscris n'a pas été lu\n"); 
        fclose(f);
        return 0;
    }
    fprintf(f2, "TETRISTE 10 PREMIERS:\n");
    int max=(nb<10) ?nb:10;
    char pseudo[MAX_PSEUDO];
    int score;
    for (int i=0;i<max;i++)
    {
        verif=fscanf(f,"%s : %d\n",pseudo,&score);
        if(verif!=2)
        {
            printf("il y a une erreur , il faut un pseudo et un score\n");
            fclose(f);
            fclose(f2);
            return 0;
        }
        fprintf(f2,"%s : %d\n" ,pseudo,score);    
    }
    fclose(f);
    fclose(f2);
    return 1;
}
