#include "terrain.h"

static void actualisation_ssListe(Terrain *T, int a_actualiser);
static void actualisation_principal(Terrain *T, int modele);
static void actualisation_ttes_ssListes(Terrain *T);


// =============== FONCTIONS STATIQUES ==================================================================================================
// ! ne verifient pas validite de leurs arguments car c'est fait dans les fonctions appelants ces fonctions statiques !
//

/// @brief Actualise la liste GENERALE selon la liste modele et reecrivant sur GENERALE. Utilisee apres la rotation pour mettre a jour le terrain (dans terrain_rotation)
/// @param T pointeur sur le Terrain a actualiser
/// @param modele Sous-liste sur laquelle on s'appuie pour mettre a jour la liste principale
/// @note complexite : lineaire (taille de la liste generale)
static void actualisation_principal(Terrain *T, int modele)
{
    Maillon *j = T->liste[GENERALE]->premier;
    Maillon *k = T->liste[modele]->premier;
    for(int i=0 ; i < T->liste[GENERALE]->taille ; i++){
        if(j->piece->coul == modele || j->piece->forme == modele){
            j->piece = k->piece;
            k = k->suiv;
        }
        j = j->suiv;
    }
}

/// @brief Actualise la liste 'a_actualiser' selon la liste GENERALE et reecrivant une nouvelle liste dans 'a_actualiser'. Utilisee apres la rotation pour mettre a jour le terrain (dans terrain_rotation)
/// @param T pointeur sur le Terrain a actualiser
/// @param a_actualiser Sous-liste que l'on met a jour, a partir de la liste principale
/// @note complexite : lineaire (taille de la liste generale)
static void actualisation_ssListe(Terrain *T, int a_actualiser)
{
    liste_libere(T->liste[a_actualiser]);
    T->liste[a_actualiser] = liste_init();
    Maillon *j = T->liste[GENERALE]->premier;
    for(int l=0 ; l < T->liste[GENERALE]->taille ; l++){
        if(j->piece->coul == a_actualiser || j->piece->forme == a_actualiser){
            liste_ajout_fin(T->liste[a_actualiser], j->piece);
        }
        j = j->suiv;
    }
}

/// @brief Pour un terrain dont la liste generale est a jour, actualise toutes ses sous-listes
/// @param T le terrain a mettre a jour a partir de sa liste generale
/// @note complexite : lineaire
inline static void actualisation_ttes_ssListes(Terrain *T)
{
    for(int coul_form=1 ; coul_form<NBRE_COUL_FORM ; coul_form++){
        actualisation_ssListe(T, coul_form);
    }
}
// ===========================================================================================================================================




/// @brief Alloue la memoire necessaire pour un terrain et toutes ses sous-listes
/// @note complexite : lineaire
/// @return Un pointeur sur le terrain cree, ou NULL en cas d'echec.
Terrain* creer_terrain()
{
    Terrain *T=malloc(sizeof(Terrain));
    if(T){
        int i, ok = 1;
        for(i=0;i<NBRE_COUL_FORM;i++)
        {
            T->liste[i] = liste_init();
            if(T->liste[i] == NULL){
                ok = 0;
                break;
            }
        }
        if(ok){
            return T;
        }
        else{
            for(int j=0 ; j<i ; j++){
                liste_libere(T->liste[j]);
            }
            free(T);
        }
    }
    return NULL;
}

/// @brief Libere le terrain T et toutes ses sous-listes
/// @note complexite : lineaire
/// @param T le terrain a liberer
void terrain_libere(Terrain *T)
{
    if(T){
        for(int i=0;i<NBRE_COUL_FORM;i++)
        {
            liste_libere(T->liste[i]);
        }
        free(T);
    }
}

/// @brief Affiche l'adresse du terrain T et le contenu de toute ses sous-listes
/// @note complexite : lineaire (taille de la liste generale)
/// @param T terrain a afficher
void affiche_terrain(Terrain *T)
{
    printf("Terrain %p :\n", T);
    if(T){
        for(int i=0;i<NBRE_COUL_FORM;i++)
        {
            printf("- %d : ", i);
            liste_affiche(T->liste[i]);
        }
    }

}

/// @brief Ajoute une piece a droite dans le terrain T (et actualise les sous-listes necessaires).
/// @param T pointeur sur un terrain
/// @param piece pointeur sur une piece a ajouter
/// @note complexite : constant
/// @return 1 en cas de reussite. -1 si un parametre etait NULL
int terrain_ajoute_droite(Terrain *T, Piece *piece)
{
    #ifdef DEBUG_TERRAIN
        printf("-- terrain_ajoute_droite : terrain %p et piece (%p)", T, piece);
    #endif
    if(T && piece){
        liste_ajout_fin(T->liste[0], piece);
        liste_ajout_fin(T->liste[piece->coul], piece);
        liste_ajout_fin(T->liste[piece->forme], piece);
        return 1;
    }
    return -1;
}

/// @brief Ajoute une piece a gauche dans le terrain T (et actualise les sous-listes necessaires).
/// @param T pointeur sur un terrain
/// @param piece pointeur sur une piece a ajouter
/// @note complexite : constant
/// @return 1 en cas de reussite. -1 si un parametre etait NULL
int terrain_ajoute_gauche(Terrain *T, Piece *piece)
{
    #ifdef DEBUG_TERRAIN
        printf("-- terrain_ajoute_gauche : terrain %p et piece (%p)", T, piece);
    #endif
    if(T && piece){
        liste_ajout_debut(T->liste[0], piece);
        liste_ajout_debut(T->liste[piece->coul], piece);
        liste_ajout_debut(T->liste[piece->forme], piece);
        return 1;
    }
    return -1;
}



/// @brief Rotation  de type 'type_piece' (peut-etre ROUGE,..., BLEU, LOSANGE,..,ROND) du terrain vers la gauche  et MAJ de toutes les listes du terrain.
/// @param T pointeur sur le terrain a rotationner
/// @param type_piece type de rotation (ROUGE,..., BLEU, LOSANGE,..,ROND)
/// @note complexite : lineaire (taille de la liste generale)
/// @return -1 si T est NULL, si c'est un mauvais type_piece, ou si la rotation est inutile. 1 sinon.
int terrain_rotation(Terrain *T, int type_piece)
{
    int autorise_rot = T && (1 <= type_piece && type_piece <= NBRE_COUL_FORM-1) && T->liste[type_piece]->taille >= 2;
    #ifdef DEBUG_TERRAIN
            printf("-- terrain_rotation de %p : (rotation de type :%d)\n", T, type_piece);
    #endif
    if(autorise_rot){
        // Rotation sous-liste du type type_piece
        liste_tourner_gauche(T->liste[type_piece]);

        // Acutalisation terrain general
        actualisation_principal(T, type_piece);

        // Actualisation des autres sous-listes a partir de la GENERALE a jour
        actualisation_ttes_ssListes(T);
        return 1;
    }
    return -1;
}

/// @brief Supprime toutes les repetitions de pieces de plus de trois formes ou couleurs consecutives. Cela reevalue les repetitions une fois une sequence supprimee en cas de creation fortuite de nouvelle sequence. Puis actualise le terrain.
/// @param T pointeur sur le terrain a examiner
/// @param suppr Tableau contenant, a la fin de la fonction, la taille de chaque sequences supprimees dans une case de tableau en laissant la premiere case vide ! (utilisee pour savoir ensuite de quel type de mouvement il s'agissait)
/// @note complexite : ???????????????????????????
/// @return Le nombre de sequence supprimee (soit la taille du tableau) ou -1 si le T est NULL.
int terrain_suppr_repet(Terrain *T, int suppr[])
{
     #ifdef DEBUG_TERRAIN
        printf("-- terrain_suppr_repet : T = %p\n", T);
    #endif
    if(T){
        int i = 0, taille_ssCh = 0, nbre_suppr = 0;
        Maillon *deb = T->liste[GENERALE]->premier;
        Maillon *fin = NULL;
        while(i < T->liste[GENERALE]->taille){
            taille_ssCh = liste_trouve_repet(T->liste[GENERALE], deb, &fin);
            if(taille_ssCh >= MIN_ALIGNEMENT){
                liste_extraire_ssChaine(T->liste[GENERALE], deb, fin, taille_ssCh);
                #ifdef DEBUG_TERRAIN
                    printf("   Extraction faite. nouvelle liste : ");
                    liste_affiche(T->liste[GENERALE]);
                #endif
                i = 0;
                deb = T->liste[GENERALE]->premier;
                suppr[++nbre_suppr] = taille_ssCh; // la case 0 est reservee au type de mouvement fait pour obtenir l'alignement (rot. ou placement piece)
            }
            else{
                deb = deb->suiv;
                i++;
            }
        }
        actualisation_ttes_ssListes(T);
        return nbre_suppr;
    }
    return -1;
}
