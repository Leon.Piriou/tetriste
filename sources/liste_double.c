/* Source : TP9 C cours E. Lazard (https://moodle.psl.eu/pluginfile.php/615047/mod_resource/content/1/tpSujets2324.pdf) */

#include "liste_double.h"

/// @brief Creation d'une liste vide
/// @note complexite : constant
/// @return Pointeur sur une liste vide (Liste*)
Liste *liste_init(){
    Liste *l = malloc(sizeof(Liste));
    if(!(l)) return NULL;

    l->premier = NULL;
    l->dernier = NULL;
    l->taille = 0;

    return l;
}

/// @brief Ajout du maillon '*p' au debut de la liste 'l'
/// @note complexite : constant
/// @return 1:succes ; 0:echec
int liste_ajout_debut(Liste *l, Piece *p){
    Maillon *m = creer_maillon(p);
    if(!(m)) return 0;

    m->suiv = l->premier;
    if(l->taille == 0){
        l->dernier = m;
    }
    else{
        l->premier->prec = m;
    }
    m->prec = l->dernier;
    l->dernier->suiv = m;

    l->premier = m;

    l->taille++;
    return 1;
}

/// @brief Ajout du maillon '*p' a la fin de la liste 'l'
/// @note complexite : constant
/// @return 1:succes ; 0:echec (pblm alloc du maillon a ajouter)
int liste_ajout_fin(Liste *l, Piece *p){
    Maillon *m = creer_maillon(p);
    if(!(m)) return 0;

    m->prec = l->dernier;
    if(l->taille == 0){
        l->premier = m;
    }
    else{
        l->dernier->suiv = m;
    }
    m->suiv = l->premier;
    l->premier->prec = m;

    l->dernier = m;

    l->taille++;
    return 1;
}

/// @brief Affichage de la liste 'l'
/// @param l liste a afficher
/// @note complexite : lineaire (taille de la liste)
/// @return void
void liste_affiche(Liste *l){
    if(l){
        Maillon *m = l->premier;
        while(m){
            piece_affiche(m->piece);
            m = m->suiv;
            // Comme on passe au suivant, on sait si on a fait un tour quand on revient au premier
            if(m == l->premier){
                break;
            }
        }
    }
    printf("\n");
}

/// @brief Extraction du premier maillon de la liste 'l'
/// @note complexite : constant
/// @return Le premier maillon (Maillon*) ou NULL en cas de liste vide
Maillon *liste_extraire_debut(Liste *l){
    Maillon *m = l->premier;
    if(!(m)) return NULL;

    if(l->taille == 1){
        l->premier = NULL;
        l->dernier = NULL;
    }
    else{
        m->suiv->prec = m->prec;
        m->prec->suiv = m->suiv;

        l->premier = m->suiv;
    }
    l->taille--;
    return m;
}

/// @brief Extraction du dernier maillon de la liste 'l'
/// @note complexite : constant
/// @return Le dernier maillon (Maillon*) ou NULL en cas de liste vide
Maillon *liste_extraire_fin(Liste *l){
    Maillon *m = l->dernier;
    if(!(m)) return NULL;

    if(l->taille == 1){
        l->premier = NULL;
        l->dernier = NULL;
    }
    else{
        m->suiv->prec = m->prec;
        m->prec->suiv = m->suiv;

        l->dernier = m->prec;
    }
    l->taille--;
    return m;
}

/// @brief Supprime la sous-liste du maillon deb au maillon fin de l. On ne verifie pas que qu'ils sont bien dans l ni dans l'ordre !
/// @param l Liste chainee de laquelle on souhaite extraire 'ssChaine'
/// @param deb Premier maillon de la sous-liste a extraire
/// @param fin Derier maillon de la sous-liste a extraire
/// @note complexite : lineaire (taille de la sous chaine)
/// @param taille_ssCh Taille de la sous-liste de 'deb' a 'fin'
void liste_extraire_ssChaine(Liste *l, Maillon *deb, Maillon *fin, int taille_ssCh){
    #ifdef DEBUG_LISTE_DOUBLE
        printf("-- liste_extraire_ssChaine\n");
    #endif
    if(l->taille >= taille_ssCh){
        l->taille -= taille_ssCh;
        #ifdef DEBUG_LISTE_DOUBLE
            printf("   La sous-chaine est de plus petite taille que la principale (l)\n"
                   "   Nouvelle taille de l = %d\n", l->taille);
        #endif

        if(l->taille){
            if(deb == l->premier){
                l->premier = fin->suiv;
            }
            if(fin == l->dernier){
                l->dernier = deb->prec;
            }
            deb->prec->suiv = fin->suiv;
            fin->suiv->prec = deb->prec;
        }
        else{
            l->premier = NULL;
            l->dernier = NULL;
            #ifdef DEBUG_LISTE_DOUBLE
                printf("   La liste principale est maintenant vide\n");
            #endif
        }
        Maillon *tmp = deb;
        for(int i=0 ; i<taille_ssCh ; i++){
            deb = deb->suiv;
            free(tmp);
            tmp = deb;
        }
        #ifdef DEBUG_LISTE_DOUBLE
            printf("   la sous-chaine est supprimee\n");
        #endif

    }
}

/// @brief Trouve la plus longue sous-chaine de repetition de la meme forme ou la meme couleur a partir du maillon actuel dans la liste l. S'arrete en fin de liste.
/// @param l Liste dans laquelle on cherche la sous-chaine
/// @param actuel maillon de l a partir duquel on cherche la sous-chaine
/// @param fin contient a la fin de la fonction le dernier maillon dans la sous-chaine
/// @note complexite : lineaire (taille de liste)
/// @return La taille de la chaine trouvee. On recupere egalement le premier maillon qui n'est pas dans cette chaine trouvee dans '*fin'
int liste_trouve_repet(Liste *l, Maillon *actuel, Maillon **fin){
    if(l && actuel){
        *fin = actuel;
        int tour = 0, meme_coul = 1, meme_forme = 1, taille = 0;

        while((!tour) && (meme_coul || meme_forme)){
            taille ++;
            *fin = (*fin)->suiv;
            if(*fin == l->premier){
                tour = 1;
            }
            if(actuel->piece->coul != (*fin)->piece->coul){
                meme_coul = 0;
            }
            if(actuel->piece->forme != (*fin)->piece->forme){
                meme_forme = 0;
            }
        }
        *fin = (*fin)->prec;
        return taille;
    }
    *fin = NULL;
    return 0;
}

/// @brief Suppression du premier element de la liste 'l' correspondant a 'x'
/// @note complexite : lineaire (taille de la liste)
/// @return Le maillon premier maillon egal a 'x' (Maillon*). NULL si on ne l'a pas trouve
Maillon *liste_supprimer(Liste *l, Piece *x){
    Maillon *m = l->premier;
    while(m){
        if(piece_compare(m->piece, x) >= 3){    // pieces identiques ou la meme
            if(l->taille == 1){
                l->premier = NULL;
                l->dernier = NULL;
            }
            else{
                if(m == l->premier){
                    l->premier = m->suiv;
                }
                else if(m == l->dernier){
                    l->dernier = m->prec;
                }
                m->prec->suiv = m->suiv;
                m->suiv->prec = m->prec;
            }
            l->taille--;
            return m;
        }
        m = m->suiv;
        // Comme on passe au suivant, on sait si on a fait un tour quand on revient au premier
        if(m == l->premier){
            break;
        }
    }
    return NULL;
}


/// @brief Decallage de tous les elements de la liste de un cran a droite
/// @note complexite : constant
void liste_tourner_droite(Liste *l){
    if(l->taille >= 2){
        l->premier = l->premier->prec;
        l->dernier = l->dernier->prec;
    }
}

/// @brief Decallage de tous les elements de la liste de un cran a gauche
/// @note complexite : constant
void liste_tourner_gauche(Liste *l){
    if(l->taille >= 2){
        l->premier = l->premier->suiv;
        l->dernier = l->dernier->suiv;
    }
}

/// @brief Liberation de la memoire contenant la liste l
/// @note complexite : lineaire (taille de la liste)
void liste_libere(Liste *l){
    Maillon *m = l->premier, *tmp = NULL;
    if(m){
        while(m != l->dernier){
            tmp = m->suiv;
            free(m);
            m = tmp;
        }
        free(m);    // liberation du dernier maillon
    }
    free(l);
}
