#include "main.h"


static void res_tests(){
    #ifdef TESTS
        printf("%s\n", test() ? "TESTS OK" : "ERREUR TESTS");
    #else
        ;
    #endif
}

int main(int argc, char *argv[]){
    // Initialisation du hasard pour tout le programme
    #ifdef DEBUG_TETRISTE
        srand(1);
    #else
        srand(time(NULL));
    #endif
    res_tests();

    Fct_EntreeSortie fonctions;
    // Si on veut jouer dans le terminal (option -t)
    if(argc == 2 && strcmp(argv[1], "-t") == 0){
        fonctions.aff_bienvenue = &term_aff_bienvenue;
        fonctions.argBienvenue = NULL;
        
        fonctions.recup_pseudo = &term_recup_pseudo;
        fonctions.argRecupPseudo = NULL;

        fonctions.question = &term_question;
        fonctions.argQuestion = NULL;

        fonctions.aff_regles = &term_aff_regles;
        fonctions.argRegles = NULL;

        fonctions.aff_partie = &term_aff_partie;
        fonctions.argJeu = NULL;

        fonctions.recup_cmd = &term_recup_cmd;
        fonctions.argRecupCmd = NULL;

        tetriste_principal(&fonctions);

    }
    else if(argc == 1){
        printf("Partie graphique a venir... Utiliser l'option -t pour jouer dans le terminal\n");
        // fonctions.aff_bienvenue = ...;
        // fonctions.recup_pseudo = ...;
        // fonctions.question = ...;
        // fonctions.aff_regles = ...;
        // fonctions.recup_cmd = ...;
    }
    else{
        printf("Usage %s :\nPas d'option -> Affichage graphique\nOption '-t' -> Jeu dans le terminal\n", argv[0]);
    }

    return 0;
}
