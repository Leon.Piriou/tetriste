#include "tetriste.h"

/// @brief Creation de toutes les pieces possibles avec lesquelles on pourra jouer et stockee dans un tableau.
/// @param ttes_pces Tableau de pieces
/// @param n_coul Nombre de couleurs voulues
/// @param n_forme Nombre de formes voulues
/// @note complexite : cubique (n_coul et n_forme)
/// @return 0 en cas d'echec de creation d'une piece, toutes les pieces precedemment cree dans le tableau sont liberee dans ce cas ; 1 si succes
int ttste_creer_ttes_pces(Piece **ttes_pces, int n_coul, int n_forme){
    for(int i=0 ; i<n_coul ; i++){
        for(int j=0 ; j<n_forme ; j++){
            if(!(ttes_pces[i*N_COUL+j] = cree_piece(i+1, j+N_COUL+1))){
                printf("-- %s : Erreur lors de la creation de toutes les pieces (i:%d j:%d)\n", __func__, i, j);
                for(int k=0 ; k<i*n_coul+j ; k++){
                    free(ttes_pces[k]);
                    ttes_pces[k] = NULL;
                }
                return 0;
            }
            #ifdef DEBUG_TETRISTE
                piece_affiche(ttes_pces[i*N_COUL+j]);
            #endif
        }
    }
    return 1;
}


/// @brief Initialise l'environnement pour le jeu. Boucle du jeu. Sauvegarde de jeu
/// @param fes Struct contenant les fonctions d'entree/sortie et leurs arguments
/// @note cubique (N_FORME et N_COUl)
/// @return 0 si erreur ; 1 succes
int tetriste_principal(Fct_EntreeSortie *fes){
// _______Variables_____________________________
    int code_r = 1;
    Piece *ttes_pces[N_COUL*N_FORME] = {0};
    char pseudo[MAX_PSEUDO] = {0};
    int placement = -1, n_joueurs = 0, est_nveau = 1;
    Jeu *jeu = NULL;
    Joueur **tout_pseudos = NULL;
    Piece *nvelle = NULL;
    int taille = 0;
    int *suppr = NULL;
    int cmd = 0, param = 0;

// ______Initialisation de la partie_______________

    // creation de toutes les pieces possibles a partir desquelles on jouera
    if(! ttste_creer_ttes_pces(ttes_pces, N_COUL, N_FORME)){    // force la complexite de toute la foction
        printf("-- %s : Erreur creation de toutes les pieces", __func__);
        code_r = 0;
        goto fin;
    }

    fes->aff_bienvenue(fes->argBienvenue);
    fes->recup_pseudo(pseudo, fes->argRecupPseudo);
    #ifdef DEBUG_TETRISTE
        printf("-- %s : pseudo recupere : %s\n", __func__, pseudo);
    #endif

    jeu = creer_jeu();
    if(jeu == NULL){
        printf("-- %s : Impossible de creer une partie\n", __func__);
        code_r = 0;
        goto fin1;
    }

    //charge tous les pseudos, regarde si deja existant:
    tout_pseudos = charge_classement(pseudo, &n_joueurs, &placement, &est_nveau);
    if(tout_pseudos == NULL){
        printf("-- %s :Echec chargement du classement\n", __func__);
        code_r = 0;
        goto fin2;
    }
    else if(est_nveau || !(fes->question("Voulez-vous ouvrir votre partie", fes->argQuestion))){ // question executee ssi palce != -1 ?
        if(!(jeu_init_liste_attente(jeu, ttes_pces, N_COUL*N_FORME))){
            printf("-- %s : Erreur lors du remplissage de la file d'attente\n", __func__);
           code_r = 0;
           goto fin3;
        }
        // met un premier element dans sur le plateau de jeu:
        nvelle = ttes_pces[(rand() % N_COUL*N_FORME)];
        if(terrain_ajoute_droite(jeu->terr, nvelle) == -1){
            printf("-- %s : Erreur ajout nouvelle piece\n", __func__);
            code_r = 0;
            goto fin3;
        }
    }
    else if( ! chargement_jeu(jeu, pseudo, ttes_pces)){
        printf("-- %s : Erreur chargment du jeu enregistre\n", __func__);
        code_r = 0;
        goto fin3;
    }


    fes->aff_regles(fes->argRegles);


// _______Partie jeu_____________________
    // equivalent a floor(..) pour les positifs. | +1 case pour stocker mouvement necessaire pour avoir ce score.
    taille = (int) (MAX_PCES / MIN_ALIGNEMENT) + 1;
    #ifdef DEBUG_TETRISTE
        printf("-- %s : taille tabl suppr:%d\n", __func__, taille);
    #endif
    suppr = malloc(sizeof(int)*taille);
    if(suppr == NULL){
        printf("-- %s : Erreur d'allocation tableau des suppressions\n", __func__);
        code_r = 0;
        goto fin3;
    }

    while(jeu->terr->liste[GENERALE]->taille <= MAX_PCES){
        #ifdef DEBUG_TETRISTE
            jeu_affiche(jeu);
        #endif
        //printf("recommandee : %s\n", aff_calcul_ia(calcul_ia(jeu)));
    
        nvelle = ttes_pces[(rand() % (N_COUL*N_FORME))];
        fes->aff_partie(jeu, fes->argJeu);
        fes->recup_cmd(&cmd, &param, fes->argRecupCmd);
        if(cmd == 0){
            break;
        }
        else if(1 <= cmd && cmd <= 3){
            if(jeu_action(jeu, cmd, param, nvelle, suppr) < 0){
                printf("-- %s : Erreur de mouvement du terrain\n", __func__);
                break;
            }
        }
        else if(cmd == 4){
            fes->aff_regles(fes->argRegles);
        }
        else{
            printf("Mauvaise commande\n");
        }
    }
    if(jeu->terr->liste[GENERALE]->taille > MAX_PCES){
        printf("Fin du jeu : nombre de piece depasse\n");
    }

// ______Sauvegarde et liberation de la memeoire___________

    // actualisation du score du joueur avant enregistrement
    tout_pseudos[placement]->score = jeu->score;
    sauvegarde_jeu(jeu, pseudo);
    joueur_sauvegarde_classement(tout_pseudos, n_joueurs);

    extraction_dix_premier();

    free(suppr);
fin3:
    libere_tab_joueur(tout_pseudos, n_joueurs);
fin2:
    jeu_libere(jeu);

fin1:
    for(int i=0 ; i<N_COUL*N_FORME ; i++){
        free(ttes_pces[i]);
    }
fin:
    return code_r;
}

/// @brief Initialise la liste d'attente du jeu J au hasard avec des pieces prises au hasard dans tab de taille n
/// @param J pointeur sur le Jeu auquel on souhaite initialiser la liste d'attente
/// @param tab Tableau de toutes les pieces possibles
/// @param n taille de tab
/// @note complexite : lineaire (taille de la liste d'attente)
/// @return 0 en cas d'erreur ; 1 si succes.
int jeu_init_liste_attente(Jeu *J, Piece **tab, int n){
    Piece *nvelle = NULL;
    for(int i=0 ; i<N_PCES_ATTENTE ; i++){
        nvelle = tab[(rand() % n)];
        if(!(liste_ajout_fin(J->prochaines_pce, nvelle))){
            #ifdef DEBUG_TETRISTE
                printf("-- %s :erreur ajout piece selectionnee a la fin de la liste\n", __func__);
            #endif
            return 0;
        }
    }
    #ifdef DEBUG_TETRISTE
        printf("-- %s : fin. on a la liste d'attente :", __func__);
        liste_affiche(J->prochaines_pce);
    #endif
    return 1;
}
