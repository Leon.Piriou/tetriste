#include "./tests/tests_principaux.h"

/// @brief Execute tous les tests
/// @return 1 si succes 0 si echec
int test(){
    int res = 1;
    res &= tests_liste_double();
    res &= tests_piece();
    res &= tests_terrain();
    res &= tests_jeu();
    return res;
}
