/* Source : modification de <https://www.lamsade.dauphine.fr/~sikora/ens/c/tp9-tests.c>
*                       et <http://www.jera.com/techinfo/jtns/jtn002.html>
*/

#include "./tests/tests_liste_double.h"


static int tests_run = 0;
static Piece p1 = {ROUGE, CARRE};
static Piece p2 = {BLEU, TRIANGLE};
static Piece p3 = {VERT, LOSANGE};
static Piece p4 = {JAUNE, ROND};
static Piece p5 = {ROUGE, TRIANGLE};

static char* test_creer_maillon() {
    Maillon* m = creer_maillon(&p1);
    mu_assert("Mauvais stockage de la valeur dans le maillon", m->piece->coul==p1.coul && m->piece->forme==p1.forme);
    mu_assert("Mauvaise initialisation du maillon", m->suiv==NULL);
    mu_assert("Mauvaise initialisation du maillon", m->prec==NULL);
    free(m);
    return 0;
}

static char* test_liste_init() {
    Liste* l = liste_init();

    mu_assert("Mauvaise initialisation pointeurs de liste", l->premier==NULL);
    mu_assert("Mauvaise initialisation pointeurs de liste", l->dernier==NULL);
    mu_assert("Mauvaise initialisation de liste", l->taille==0);

    free(l);
    return 0;
}

static char* test_liste_ajout_debut() {
    Liste* l = liste_init();

    liste_ajout_debut(l, &p1);
    mu_assert("erreur taille de liste après 1 ajout", l->taille==1);
    mu_assert("erreur 1 pointeurs liste apres 1 ajout", l->premier==l->dernier);
    mu_assert("erreur 2 pointeurs liste apres 1 ajout", l->premier!=NULL);
    mu_assert("erreur 3 pointeurs liste apres 1 ajout", l->dernier!=NULL);
    mu_assert("erreur 4 pointeurs liste apres 1 ajout", l->premier->prec==l->dernier);
    mu_assert("erreur 5 pointeurs liste apres 1 ajout", l->dernier->suiv==l->dernier);
    mu_assert("erreur 6 pointeurs liste apres 1ajout", l->premier->suiv==l->dernier);
    mu_assert("erreur 7 pointeurs liste apres 1 ajout", l->dernier->prec==l->dernier);

    Maillon* old_prem = l->premier;

    liste_ajout_debut(l, &p2);
    mu_assert("erreur taille de liste après ajout", l->taille==2);
    mu_assert("erreur maillon", l->premier->piece->coul==p2.coul && l->premier->piece->forme==p2.forme);
    mu_assert("erreur maillon", l->dernier->piece->coul==p1.coul && l->dernier->piece->forme==p1.forme);
    mu_assert("erreur 1 pointeurs liste apres 2 ajouts", l->premier!=NULL);
    mu_assert("erreur 2 pointeurs liste apres 2 ajouts", l->dernier!=NULL);
    mu_assert("erreur 3 pointeurs liste apres 2 ajouts", l->premier->prec==l->dernier);
    mu_assert("erreur 4 pointeurs liste apres 2 ajouts", l->dernier->suiv==l->premier);
    mu_assert("erreur 5 pointeurs liste apres 2 ajouts", l->premier->suiv!=NULL);
    mu_assert("erreur 6 pointeurs liste apres 2 ajouts", l->dernier->prec!=NULL);
    mu_assert("erreur 7 pointeurs liste apres 2 ajouts", l->premier!=l->dernier);
    mu_assert("erreur 8 pointeurs liste apres 2 ajouts", l->premier!=NULL);
    mu_assert("erreur 9 pointeurs liste apres 2 ajouts", l->dernier!=NULL);
    mu_assert("erreur 10 pointeurs liste apres 2 ajouts", l->premier->prec==l->dernier);
    mu_assert("erreur 11 pointeurs liste apres 2 ajouts", l->dernier->suiv==l->premier);
    mu_assert("erreur 12 pointeurs liste apres 2 ajouts", l->premier->suiv==l->dernier);
    mu_assert("erreur 13 pointeurs liste apres 2 ajouts", l->dernier->prec==l->premier);
    mu_assert("erreur 14 pointeurs apres 2 ajouts", l->premier->suiv == old_prem);
    mu_assert("erreur 15 pointeurs apres 2 ajouts", old_prem->prec == l->premier);
    

    liste_libere(l);
    return 0;
}

static char* test_liste_ajout_fin() {
    Liste* l = liste_init();

    liste_ajout_fin(l, &p1);
    mu_assert("erreur taille de liste après 1 ajout fin", l->taille==1);
    mu_assert("erreur 1 pointeurs liste apres 1 ajout fin", l->premier==l->dernier);
    mu_assert("erreur 2 pointeurs liste apres 1 ajout fin", l->premier!=NULL);
    mu_assert("erreur 3 pointeurs liste apres 1 ajout fin", l->dernier!=NULL);
    mu_assert("erreur 4 pointeurs liste apres 1 ajout fin", l->premier->prec==l->dernier);
    mu_assert("erreur 5 pointeurs liste apres 1 ajout fin", l->dernier->suiv==l->dernier);
    mu_assert("erreur 6 pointeurs liste apres 1ajout fin", l->premier->suiv==l->dernier);
    mu_assert("erreur 7 pointeurs liste apres 1 ajout fin", l->dernier->prec==l->dernier);
    Maillon* old_prem = l->premier;

    liste_ajout_fin(l, &p2);
    mu_assert("erreur taille de liste après ajout fin", l->taille==2);
    mu_assert("erreur maillon fin", l->premier->piece->coul==p1.coul && l->premier->piece->forme==p1.forme);
    mu_assert("erreur maillon fin", l->dernier->piece->coul==p2.coul && l->dernier->piece->forme==p2.forme);
    mu_assert("erreur 1 pointeurs liste apres 2 ajouts fin", l->premier!=NULL);
    mu_assert("erreur 2 pointeurs liste apres 2 ajouts fin", l->dernier!=NULL);
    mu_assert("erreur 3 pointeurs liste apres 2 ajouts fin", l->premier->prec==l->dernier);
    mu_assert("erreur 4 pointeurs liste apres 2 ajouts fin", l->dernier->suiv==l->premier);
    mu_assert("erreur 5 pointeurs liste apres 2 ajouts fin", l->premier->suiv!=NULL);
    mu_assert("erreur 6 pointeurs liste apres 2 ajouts fin", l->dernier->prec!=NULL);
    mu_assert("erreur 7 pointeurs liste apres 2 ajouts fin", l->premier!=l->dernier);
    mu_assert("erreur 8 pointeurs liste apres 2 ajouts fin", l->premier!=NULL);
    mu_assert("erreur 9 pointeurs liste apres 2 ajouts fin", l->dernier!=NULL);
    mu_assert("erreur 10 pointeurs liste apres 2 ajouts fin", l->premier->prec==l->dernier);
    mu_assert("erreur 11 pointeurs liste apres 2 ajouts fin", l->dernier->suiv==l->premier);
    mu_assert("erreur 12 pointeurs liste apres 2 ajouts fin", l->premier->suiv==l->dernier);
    mu_assert("erreur 13 pointeurs liste apres 2 ajouts fin", l->dernier->prec==l->premier);
    mu_assert("erreur 14 pointeurs apres 2 ajouts fin", l->dernier->prec == old_prem);
    mu_assert("erreur 15 pointeurs apres 2 ajouts fin", old_prem->suiv == l->dernier);

    liste_libere(l);
    return 0;
}

static char* test_liste_extraire_debut() {
    Liste* l = liste_init();

    Maillon* m = liste_extraire_debut(l);
    mu_assert("Erreur extraire liste vide", m==NULL);

    liste_ajout_debut(l,&p1);
    Maillon* tete=l->premier;
    m = liste_extraire_debut(l);
    mu_assert("Erreur 1 extraire debut", m==tete);
    mu_assert("Erreur 2 extraire debut", l->taille == 0);
    mu_assert("Erreur 3 extraire debut", l->dernier == NULL);
    mu_assert("Erreur 4 extraire debut", l->premier == NULL);
    free(m);

    liste_ajout_debut(l, &p1);
    liste_ajout_debut(l, &p2);
    tete=l->premier;
    m = liste_extraire_debut(l);
    mu_assert("Erreur 1 extraire debut 2 ajouts", m==tete);
    mu_assert("Erreur 2 extraire debut 2 ajouts", l->taille == 1);
    mu_assert("Erreur 3 extraire debut 2 ajouts", l->dernier == l->premier);
    mu_assert("Erreur 4 extraire debut 2 ajouts", l->premier->prec == l->dernier);

    free(m);

    liste_libere(l);
    return 0;
}

static char* test_liste_extraire_fin() {
    Liste* l = liste_init();

    Maillon* m = liste_extraire_fin(l);
    mu_assert("Erreur extraire fin liste vide", m==NULL);

    liste_ajout_debut(l, &p1);
    Maillon* tete=l->premier;
    m = liste_extraire_fin(l);
    mu_assert("Erreur 1 extraire fin", m==tete);
    mu_assert("Erreur 2 extraire fin", l->taille == 0);
    mu_assert("Erreur 3 extraire fin", l->dernier == NULL);
    mu_assert("Erreur 4 extraire fin", l->premier == NULL);
    free(m);

    liste_ajout_debut(l, &p1);
    liste_ajout_debut(l, &p2);
    Maillon* fin=l->dernier;
    m = liste_extraire_fin(l);
    mu_assert("Erreur 1 extraire fin 2 ajouts", m==fin);
    mu_assert("Erreur 2 extraire fin 2 ajouts", l->taille == 1);
    mu_assert("Erreur 3 extraire fin 2 ajouts", l->dernier == l->premier);
    mu_assert("Erreur 4 extraire fin 2 ajouts", l->dernier->suiv == l->premier);

    free(m);

    liste_libere(l);
    return 0;
}

static char* test_liste_supprimer() {
    Liste* l = liste_init();

    liste_ajout_debut(l, &p1);
    Maillon* m = liste_supprimer(l, &p1);
    mu_assert("Erreur 1 liste supprimer 1", l->taille == 0);
    mu_assert("Erreur 2 liste supprimer 1", l->dernier == NULL);
    mu_assert("Erreur 3 liste supprimer 1", l->premier == NULL);
    free(m);

    liste_ajout_debut(l, &p1);
    liste_ajout_debut(l, &p2);
    liste_ajout_debut(l, &p3);

    m = liste_supprimer(l, &p4);

    mu_assert("Erreur 1 liste supprimer", m==NULL);
    
    Maillon* tete = l->premier;
    m = liste_supprimer(l, &p3);
    mu_assert("erreur 2 liste supprimer", tete==m);
    mu_assert("erreur 3 liste supprimer", l->premier->prec==l->dernier);
    mu_assert("erreur 4 liste supprimer", l->taille==2);
    mu_assert("erreur 5 liste supprimer", tete->suiv==l->premier);    
    free(m);

    liste_ajout_debut(l, &p3);
    m = liste_supprimer(l, &p2);

    mu_assert("erreur 6 liste supprimer", l->premier->suiv == l->dernier);
    mu_assert("erreur 7 liste supprimer", l->premier->suiv == l->dernier);
    mu_assert("erreur 8 liste supprimer", l->dernier->prec == l->premier);
    mu_assert("erreur 9 liste supprimer", l->taille == 2);

    free(m);

    m = liste_supprimer(l, &p1);
    mu_assert("erreur 10 liste supprimer", l->dernier == l->premier);
    mu_assert("erreur 11 liste supprimer", l->taille == 1);
    mu_assert("erreur 12 liste supprimer", liste_supprimer(l, &p1) == NULL);
    free(m);

    liste_libere(l);
    return 0;
}

static char* test_liste_tourner_droite() {
    Liste* l = liste_init();
    liste_ajout_debut(l, &p5);
    liste_ajout_debut(l, &p4);
    liste_ajout_debut(l, &p3);
    liste_ajout_debut(l, &p2);
    liste_ajout_debut(l, &p1);

    Maillon* old_dernier = l->dernier;
    Maillon* old_premier = l->premier;

    liste_tourner_droite(l);
    mu_assert("Erreur 1 liste tourne droite", l->premier->piece->coul == p5.coul && l->premier->piece->forme == p5.forme);
    mu_assert("Erreur 2 liste tourne droite", l->dernier->piece->coul == p4.coul && l->dernier->piece->forme == p4.forme);
    mu_assert("Erreur 3 liste tourne droite", l->premier->prec == l->dernier);
    mu_assert("Erreur 4 liste tourne droite", l->dernier->suiv == l->premier);
    mu_assert("Erreur 5 liste tourne droite", l->premier->prec == l->dernier);
    mu_assert("Erreur 6 liste tourne droite", old_dernier->suiv == old_premier);
    mu_assert("Erreur 7 liste tourne droite", old_premier->prec == old_dernier);
    mu_assert("Erreur 8 liste tourne droite", l->taille == 5);

    liste_libere(l);
    return 0;
}

static char* test_liste_tourner_gauche() {
    Liste* l = liste_init();
    liste_ajout_debut(l, &p5);
    liste_ajout_debut(l, &p4);
    liste_ajout_debut(l, &p3);
    liste_ajout_debut(l, &p2);
    liste_ajout_debut(l, &p1);

    Maillon* old_dernier = l->dernier;
    Maillon* old_premier = l->premier;
    liste_tourner_gauche(l);
    mu_assert("Erreur 1 liste tourne droite", l->premier->piece->coul == p2.coul && l->premier->piece->forme == p2.forme);
    mu_assert("Erreur 2 liste tourne droite", l->dernier->piece->coul == p1.coul && l->dernier->piece->forme == p1.forme);
    mu_assert("Erreur 3 liste tourne droite", l->premier->prec == l->dernier);
    mu_assert("Erreur 4 liste tourne droite", l->dernier->suiv == l->premier);
    mu_assert("Erreur 5 liste tourne droite", l->premier->prec == l->dernier);
    mu_assert("Erreur 6 liste tourne droite", old_dernier->suiv == old_premier);
    mu_assert("Erreur 7 liste tourne droite", old_premier->prec == old_dernier);
    mu_assert("Erreur 8 liste tourne droite", l->taille == 5);

    liste_libere(l);
    return 0;
}

static char *all_tests() {        
    mu_run_test(test_creer_maillon);
    mu_run_test(test_liste_init);
    mu_run_test(test_liste_ajout_debut);
    mu_run_test(test_liste_ajout_fin);
    mu_run_test(test_liste_extraire_debut);
    mu_run_test(test_liste_extraire_fin);
    mu_run_test(test_liste_supprimer);
    mu_run_test(test_liste_tourner_droite);
    mu_run_test(test_liste_tourner_gauche);
    return 0;
}

int tests_liste_double()
{
    char *result = all_tests();
    printf("--tests liste_double.c: ");
    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("\t\tTests run: %d\n", tests_run);

    return result == 0;
}

