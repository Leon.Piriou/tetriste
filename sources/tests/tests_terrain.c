/* Source :  <http://www.jera.com/techinfo/jtns/jtn002.html> */

#include "tests/tests_terrain.h"


static int tests_run = 0;


static Piece p5 = {ROUGE, CARRE};

static Piece CR = {ROUGE, CARRE};
static Piece CV = {VERT, CARRE};

static Piece LJ = {JAUNE, LOSANGE};

static Piece TB = {BLEU, TRIANGLE};
static Piece TJ = {JAUNE, TRIANGLE};
static Piece TR = {ROUGE, TRIANGLE};

static Piece RV = {VERT, ROND};
static Piece RR = {ROUGE, ROND};
static Piece RJ = {JAUNE, ROND};


static char *test_cree_terrain(){
    Terrain *T = creer_terrain();
    mu_assert("cree_terrain: err1", sizeof(*T) == sizeof(Terrain));
    for(int i=0 ; i<NBRE_COUL_FORM ; i++){
        mu_assert("cree_terrain : err2", sizeof(Liste) == sizeof(*(T->liste[i])));
        mu_assert("cree_terrain : err3", T->liste[i]->premier == NULL);
        mu_assert("cree_terrain : err4", T->liste[i]->dernier == NULL);
        mu_assert("cree_terrain : err5", T->liste[i]->taille == 0);
    }
    
    terrain_libere(T);
    return 0;
}

static char *test_terrain_ajoute_droite(){
    Terrain *T = creer_terrain();

    terrain_ajoute_droite(T, &CR); // carre rouge
    mu_assert("terrain_ajoute_droite : err1", piece_compare(T->liste[GENERALE]->premier->piece, &p5) == 3); // p5:carre rouge
    mu_assert("terrain_ajoute_droite : err2", piece_compare(T->liste[GENERALE]->premier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_droite : err3", piece_compare(T->liste[GENERALE]->premier->piece, T->liste[0]->premier->suiv->piece) == 4);
    mu_assert("terrain_ajoute_droite : err4", piece_compare(T->liste[GENERALE]->premier->piece, T->liste[0]->premier->prec->piece) == 4);
    mu_assert("terrain_ajoute_droite : err5", piece_compare(T->liste[CR.coul]->premier->piece, &p5) == 3); // p5:carre rouge
    mu_assert("terrain_ajoute_droite : err6", piece_compare(T->liste[CR.coul]->premier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_droite : err7", piece_compare(T->liste[CR.coul]->premier->piece, T->liste[CR.coul]->premier->suiv->piece) == 4);
    mu_assert("terrain_ajoute_droite : err8", piece_compare(T->liste[CR.coul]->premier->piece, T->liste[CR.coul]->premier->prec->piece) == 4);
    mu_assert("terrain_ajoute_droite : err9", piece_compare(T->liste[CR.forme]->premier->piece, &p5) == 3); // p5:carre rouge
    mu_assert("terrain_ajoute_droite : err10", piece_compare(T->liste[CR.forme]->premier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_droite : err11", piece_compare(T->liste[CR.forme]->premier->piece, T->liste[CR.forme]->premier->suiv->piece) == 4);
    mu_assert("terrain_ajoute_droite : err12", piece_compare(T->liste[CR.forme]->premier->piece, T->liste[CR.forme]->premier->prec->piece) == 4);

    terrain_ajoute_droite(T, &TJ); // triangle jaune
    mu_assert("terrain_ajoute_droite : err13", piece_compare(T->liste[GENERALE]->premier->suiv->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_droite : err14", piece_compare(T->liste[GENERALE]->premier->prec->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_droite : err15", piece_compare(T->liste[TJ.coul]->premier->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_droite : err16", piece_compare(T->liste[TJ.forme]->premier->piece, &TJ) == 4);

    terrain_ajoute_droite(T, &TR); // triangle rouge
    mu_assert("terrain_ajoute_droite : err17", piece_compare(T->liste[GENERALE]->premier->suiv->suiv->piece, &TR) == 4);
    mu_assert("terrain_ajoute_droite : err18", piece_compare(T->liste[GENERALE]->dernier->piece, &TR) == 4);
    mu_assert("terrain_ajoute_droite : err19", piece_compare(T->liste[GENERALE]->premier->prec->piece, &TR) == 4);
    mu_assert("terrain_ajoute_droite : err20", piece_compare(T->liste[TR.coul]->premier->suiv->piece, &TR) == 4);
    mu_assert("terrain_ajoute_droite : err21", piece_compare(T->liste[TR.forme]->premier->suiv->piece, &TR) == 4);
    mu_assert("terrain_ajoute_droite : err22", piece_compare(T->liste[TR.coul]->dernier->piece, &TR) == 4);
    mu_assert("terrain_ajoute_droite : err23", piece_compare(T->liste[TR.forme]->dernier->piece, &TR) == 4);

    mu_assert("terrain_ajoute_droite : err24", T->liste[GENERALE]->taille == 3);
    mu_assert("terrain_ajoute_droite : err25", T->liste[ROUGE]->taille == 2);
    mu_assert("terrain_ajoute_droite : err26", T->liste[TRIANGLE]->taille == 2);
    mu_assert("terrain_ajoute_droite : err27", T->liste[JAUNE]->taille == 1);
    mu_assert("terrain_ajoute_droite : err28", T->liste[CARRE]->taille == 1);
    mu_assert("terrain_ajoute_droite : err29", T->liste[VERT]->taille == 0);
    mu_assert("terrain_ajoute_droite : err30", T->liste[BLEU]->taille == 0);
    mu_assert("terrain_ajoute_droite : err31", T->liste[LOSANGE]->taille == 0);
    mu_assert("terrain_ajoute_droite : err32", T->liste[ROND]->taille == 0);    

    return 0;
}

static char *test_terrain_ajoute_gauche(){
    Terrain *T = creer_terrain();

    terrain_ajoute_gauche(T, &CR); // carre rouge
    mu_assert("terrain_ajoute_gauche : err1", piece_compare(T->liste[GENERALE]->premier->piece, &p5) == 3); // p5:carre rouge
    mu_assert("terrain_ajoute_gauche : err2", piece_compare(T->liste[GENERALE]->premier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err3", piece_compare(T->liste[GENERALE]->premier->piece, T->liste[0]->premier->suiv->piece) == 4);
    mu_assert("terrain_ajoute_gauche : err4", piece_compare(T->liste[GENERALE]->premier->piece, T->liste[0]->premier->prec->piece) == 4);
    mu_assert("terrain_ajoute_gauche : err5", piece_compare(T->liste[CR.coul]->premier->piece, &p5) == 3); // p5:carre rouge
    mu_assert("terrain_ajoute_gauche : err6", piece_compare(T->liste[CR.coul]->premier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err7", piece_compare(T->liste[CR.coul]->premier->piece, T->liste[CR.coul]->premier->suiv->piece) == 4);
    mu_assert("terrain_ajoute_gauche : err8", piece_compare(T->liste[CR.coul]->premier->piece, T->liste[CR.coul]->premier->prec->piece) == 4);
    mu_assert("terrain_ajoute_gauche : err9", piece_compare(T->liste[CR.forme]->premier->piece, &p5) == 3); // p5:carre rouge
    mu_assert("terrain_ajoute_gauche : err10", piece_compare(T->liste[CR.forme]->premier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err11", piece_compare(T->liste[CR.forme]->premier->piece, T->liste[CR.forme]->premier->suiv->piece) == 4);
    mu_assert("terrain_ajoute_gauche : err12", piece_compare(T->liste[CR.forme]->premier->piece, T->liste[CR.forme]->premier->prec->piece) == 4);

    terrain_ajoute_gauche(T, &TJ); // triangle jaune
    mu_assert("terrain_ajoute_gauche : err13", piece_compare(T->liste[GENERALE]->premier->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_gauche : err14", piece_compare(T->liste[GENERALE]->premier->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_gauche : err13 bis", piece_compare(T->liste[GENERALE]->premier->suiv->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err14 bis", piece_compare(T->liste[GENERALE]->premier->suiv->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err15", piece_compare(T->liste[TJ.coul]->premier->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_gauche : err16", piece_compare(T->liste[TJ.forme]->premier->piece, &TJ) == 4);

    terrain_ajoute_gauche(T, &TR); // triangle rouge
    mu_assert("terrain_ajoute_gauche : err17", piece_compare(T->liste[GENERALE]->premier->suiv->suiv->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err18", piece_compare(T->liste[GENERALE]->dernier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err19", piece_compare(T->liste[GENERALE]->premier->prec->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err20", piece_compare(T->liste[TR.coul]->premier->suiv->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err21", piece_compare(T->liste[TR.forme]->premier->suiv->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_gauche : err22", piece_compare(T->liste[TR.coul]->dernier->piece, &CR) == 4);
    mu_assert("terrain_ajoute_gauche : err23", piece_compare(T->liste[TR.forme]->dernier->piece, &TJ) == 4);
    mu_assert("terrain_ajoute_gauche : err17 bis", piece_compare(T->liste[GENERALE]->premier->piece, &TR) == 4);
    mu_assert("terrain_ajoute_gauche : err18 bis", piece_compare(T->liste[GENERALE]->dernier->suiv->piece, &TR) == 4);
    mu_assert("terrain_ajoute_gauche : err19 bis", piece_compare(T->liste[GENERALE]->premier->piece, &TR) == 4);
    mu_assert("terrain_ajoute_gauche : err20 bis", piece_compare(T->liste[TR.coul]->premier->piece, &TR) == 4);
    mu_assert("terrain_ajoute_gauche : err21 bis", piece_compare(T->liste[TR.forme]->premier->piece, &TR) == 4);
    mu_assert("terrain_ajoute_gauche : err22 bis", piece_compare(T->liste[TR.coul]->dernier->suiv->piece, &TR) == 4);
    mu_assert("terrain_ajoute_gauche : err23 bis", piece_compare(T->liste[TR.forme]->dernier->suiv->piece, &TR) == 4);


    mu_assert("terrain_ajoute_gauche : err24", T->liste[GENERALE]->taille == 3);
    mu_assert("terrain_ajoute_gauche : err25", T->liste[ROUGE]->taille == 2);
    mu_assert("terrain_ajoute_gauche : err26", T->liste[TRIANGLE]->taille == 2);
    mu_assert("terrain_ajoute_gauche : err27", T->liste[JAUNE]->taille == 1);
    mu_assert("terrain_ajoute_gauche : err28", T->liste[CARRE]->taille == 1);
    mu_assert("terrain_ajoute_gauche : err29", T->liste[VERT]->taille == 0);
    mu_assert("terrain_ajoute_gauche : err30", T->liste[BLEU]->taille == 0);
    mu_assert("terrain_ajoute_gauche : err31", T->liste[LOSANGE]->taille == 0);
    mu_assert("terrain_ajoute_gauche : err32", T->liste[ROND]->taille == 0);    

    return 0;
}

static char *test_terrain_rotation(){
    Terrain *l_terrains[10] = {NULL};

    l_terrains[0] = creer_terrain();
    l_terrains[1] = creer_terrain();
    terrain_ajoute_droite(l_terrains[1], &CR);
    l_terrains[2] = creer_terrain();
    terrain_ajoute_droite(l_terrains[2], &CR);
    terrain_ajoute_droite(l_terrains[2], &CR);
    l_terrains[3] = creer_terrain();
    terrain_ajoute_droite(l_terrains[3], &CR);
    terrain_ajoute_droite(l_terrains[3], &TJ);
    l_terrains[4] = creer_terrain();
    terrain_ajoute_droite(l_terrains[4], &CR);
    terrain_ajoute_droite(l_terrains[4], &TR);
    terrain_ajoute_droite(l_terrains[4], &CR);
    l_terrains[5] = creer_terrain();
    terrain_ajoute_droite(l_terrains[5], &TJ);
    terrain_ajoute_droite(l_terrains[5], &CR);
    terrain_ajoute_droite(l_terrains[5], &CR);
    l_terrains[6] = creer_terrain();
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &TR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    l_terrains[7] = creer_terrain();
    terrain_ajoute_droite(l_terrains[7], &CR);
    terrain_ajoute_droite(l_terrains[7], &LJ);
    terrain_ajoute_droite(l_terrains[7], &TB);
    terrain_ajoute_droite(l_terrains[7], &RV);
    terrain_ajoute_droite(l_terrains[7], &CV);

    int r = terrain_rotation(l_terrains[0], 1);
    mu_assert("terrain rotation : err1", r == -1);

    r = terrain_rotation(l_terrains[7], 10);
    mu_assert("terrain rotation : err2", r == -1);

    r = terrain_rotation(l_terrains[7], 1);
    mu_assert("terrain rotation : err3", r == -1);

    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[7]);
    #endif
    r = terrain_rotation(l_terrains[7], CARRE);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[7]);
    #endif
    mu_assert("terrain rotation : err2", r == 1);
    mu_assert("terrain_rotation : err3", l_terrains[7]->liste[GENERALE]->dernier->piece == &CR);
    mu_assert("terrain_rotation : err4", l_terrains[7]->liste[GENERALE]->dernier->prec->piece == &RV);
    mu_assert("terrain_rotation : err3", l_terrains[7]->liste[GENERALE]->premier->piece == &CV);
    mu_assert("terrain_rotation : err3", l_terrains[7]->liste[GENERALE]->premier->suiv->piece == &LJ);
    mu_assert("terrain_rotation : err3", l_terrains[7]->liste[GENERALE]->premier->suiv->suiv->piece == &TB);

    mu_assert("terrain_rotation : err3", l_terrains[7]->liste[VERT]->premier->piece == &CV);
    mu_assert("terrain_rotation : err3", l_terrains[7]->liste[VERT]->dernier->piece == &RV);


    for(int i=0 ; i<10 ; i++){
        terrain_libere(l_terrains[i]);
    }

    return 0;
}

static char *test_terrain_suppr_repet(){
    Terrain *l_terrains[10] = {NULL};
    int suppr[10] = {0};
    int ref[10] = {0};

    
    l_terrains[0] = creer_terrain();
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[0]);
    #endif
    int r = terrain_suppr_repet(l_terrains[0], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[0]);
    #endif
    mu_assert("terrain_suppr_repet : err1", r == 0 && memcmp(suppr, ref, 10) == 0);


    l_terrains[1] = creer_terrain();
    terrain_ajoute_droite(l_terrains[1], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[1]);
    #endif
    r = terrain_suppr_repet(l_terrains[1], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[1]);
    #endif
    mu_assert("terrain_suppr_repet : err2", r == 0 && memcmp(suppr, ref, 10) == 0);


    l_terrains[2] = creer_terrain();
    terrain_ajoute_droite(l_terrains[2], &CR);
    terrain_ajoute_droite(l_terrains[2], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[2]);
    #endif
    r = terrain_suppr_repet(l_terrains[2], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[2]);
    #endif
    mu_assert("terrain_suppr_repet : err3", r == 0 && memcmp(suppr, ref, 10) == 0);

    l_terrains[3] = creer_terrain();
    terrain_ajoute_droite(l_terrains[3], &CR);
    terrain_ajoute_droite(l_terrains[3], &TJ);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[3]);
    #endif
    r = terrain_suppr_repet(l_terrains[3], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[3]);
    #endif
    mu_assert("terrain_suppr_repet : err4", r == 0 && memcmp(suppr, ref, 10) == 0);


    l_terrains[4] = creer_terrain();
    terrain_ajoute_droite(l_terrains[4], &CR);
    terrain_ajoute_droite(l_terrains[4], &TR);
    terrain_ajoute_droite(l_terrains[4], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[4]);
    #endif
    r = terrain_suppr_repet(l_terrains[4], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[4]);
    #endif
    mu_assert("terrain_suppr_repet : err5", r == 1 && suppr[1] == 3 && suppr[2] == 0);
    memmove(suppr, ref, 10);


    l_terrains[5] = creer_terrain();
    terrain_ajoute_droite(l_terrains[5], &TJ);
    terrain_ajoute_droite(l_terrains[5], &CR);
    terrain_ajoute_droite(l_terrains[5], &CR);
    terrain_ajoute_droite(l_terrains[5], &TR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[5]);
    #endif
    r = terrain_suppr_repet(l_terrains[5], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[5]);
    #endif
    mu_assert("terrain_suppr_repet : err6", r == 1 && suppr[1] == 3 && suppr[2] == 0);
    memmove(suppr, ref, 10);

    l_terrains[6] = creer_terrain();
    terrain_ajoute_droite(l_terrains[6], &TR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &TJ);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    r = terrain_suppr_repet(l_terrains[6], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    mu_assert("terrain_supprrepet : err7", r == 1 && suppr[1] == 3 && suppr[2] == 0);
    memmove(suppr, ref, 10);


    terrain_libere(l_terrains[6]);
    l_terrains[6] = creer_terrain();
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &TR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    r = terrain_suppr_repet(l_terrains[6], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    mu_assert("terrain_suppr_repet : err8", r == 1 && suppr[1] == 4 && suppr[2] == 0);
    memmove(suppr, ref, 10);

    

    l_terrains[7] = creer_terrain();
    terrain_ajoute_droite(l_terrains[7], &RR);
    terrain_ajoute_droite(l_terrains[7], &RJ);
    terrain_ajoute_droite(l_terrains[7], &TB);
    terrain_ajoute_droite(l_terrains[7], &CR);
    terrain_ajoute_droite(l_terrains[7], &TR);
    terrain_ajoute_droite(l_terrains[7], &RR);
    terrain_ajoute_droite(l_terrains[7], &TJ);
    terrain_ajoute_droite(l_terrains[7], &TR);
    terrain_ajoute_droite(l_terrains[7], &RV);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[7]);
    #endif
    r = terrain_suppr_repet(l_terrains[7], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[7]);
    #endif
    mu_assert("terrain_suppr_repet : err9", r == 3 && l_terrains[7]->liste[GENERALE]->taille == 0 && suppr[1] == 3 && suppr[2] == 3 && suppr[3] == 3 && suppr[4] == 0);
    memmove(suppr, ref, 10);

    terrain_libere(l_terrains[6]);
    l_terrains[6] = creer_terrain();
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &TB);
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    r = terrain_suppr_repet(l_terrains[6], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    mu_assert("terrain_suppr_repet : err10", r == 0 && memcmp(suppr, ref, 10) == 0 && l_terrains[6]->liste[GENERALE]->taille == 4);
    memmove(suppr, ref, 10);

    terrain_libere(l_terrains[6]);
    l_terrains[6] = creer_terrain();
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &TR);
    terrain_ajoute_droite(l_terrains[6], &CV);
    terrain_ajoute_droite(l_terrains[6], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    r = terrain_suppr_repet(l_terrains[6], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    mu_assert("terrain_suppr_repet : err11", r == 0 && memcmp(suppr, ref, 10) == 0 && l_terrains[6]->liste[GENERALE]->taille == 4);
    memmove(suppr, ref, 10);

    terrain_libere(l_terrains[6]);
    l_terrains[6] = creer_terrain();
    terrain_ajoute_droite(l_terrains[6], &CR);
    terrain_ajoute_droite(l_terrains[6], &TR);
    terrain_ajoute_droite(l_terrains[6], &CV);
    terrain_ajoute_droite(l_terrains[6], &CR);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    r = terrain_suppr_repet(l_terrains[6], suppr);
    #ifdef DEBUG_TESTS_TERRAINS
        affiche_terrain(l_terrains[6]);
    #endif
    mu_assert("terrain_suppr_repet : err8", r == 0 && memcmp(suppr, ref, 10) == 0 && l_terrains[6]->liste[GENERALE]->taille == 4);
    memmove(suppr, ref, 10);

    for(int i=0 ; i<10 ; i++){
        terrain_libere(l_terrains[i]);
    }
    return 0;
}

static char *all_tests(){      
    mu_run_test(test_cree_terrain);
    mu_run_test(test_terrain_ajoute_droite);
    mu_run_test(test_terrain_ajoute_gauche);
    mu_run_test(test_terrain_rotation);
    mu_run_test(test_terrain_suppr_repet);
    return 0;
}

int tests_terrain()
{
    char *result = all_tests();
    printf("--tests terrain.c: ");

    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("\t\tTests run: %d\n", tests_run);

    return result == 0;
}
