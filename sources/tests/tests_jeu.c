/* Source :  <http://www.jera.com/techinfo/jtns/jtn002.html> */

#include "./tests/tests_jeu.h"


static int tests_run = 0;

static Piece CR = {ROUGE, CARRE};
static Piece CV = {VERT, CARRE};
static Piece CJ = {JAUNE, CARRE};
static Piece CB = {BLEU, CARRE};

static Piece TV = {VERT, TRIANGLE};
static Piece * t[5] = {&CR, &CV, &CJ ,&CB, &TV};

static Piece RJ = {JAUNE, ROND};


static void rempli_liste_attente(Jeu *J, int n){
    // n <= 5
    for(int i=0 ; i<n ; i++){
        liste_ajout_fin(J->prochaines_pce, t[i]);
    }
}

static char *test_creer_jeu(){
    Jeu *J = creer_jeu();
    mu_assert("creer_jeu : err1", sizeof(*J) == sizeof(Jeu));
    mu_assert("creer_jeu : err2", 0 == J->score);
    mu_assert("creer_jeu : err2", sizeof(*(J->prochaines_pce)) == sizeof(Liste));
    mu_assert("creer_jeu : err2", sizeof(*(J->terr)) == sizeof(Terrain));
    jeu_libere(J);
    
    return 0;
}

static char *test_jeu_action(){
    Maillon *nouv_a_sortir = NULL;
    // ajout droite :
    for(int i = 0 ; i<N_PCES_ATTENTE ; i++){    // On teste pour toute taille de file d'attente
        Jeu *J = creer_jeu();
        int suppr[15] = {0};
        rempli_liste_attente(J, i+1);
        
        if(i != 0){
            nouv_a_sortir = J->prochaines_pce->dernier->prec;
        }
        int r = jeu_action(J, 1, 0, &RJ, suppr);
        if(i != 0){
            mu_assert("jeu_mouvement (d) : err1", J->prochaines_pce->dernier == nouv_a_sortir);
        }
        else{
            mu_assert("jeu_mouvement (d) : err1", J->prochaines_pce->dernier->piece == &RJ);
        }
        mu_assert("jeu_mouvement (d) : err2", r && J->prochaines_pce->premier->piece == &RJ);
        mu_assert("jeu_mouvement (d) : err3", r && J->terr->liste[GENERALE]->dernier->piece == t[i]);
        jeu_libere(J);
    }
    // ajout gauche :
    for(int i = 0 ; i<N_PCES_ATTENTE ; i++){    // On teste pour toute taille de file d'attente
        Jeu *J = creer_jeu();
        int suppr[15] = {0};
        rempli_liste_attente(J, i+1);
        
        if(i != 0){
            nouv_a_sortir = J->prochaines_pce->dernier->prec;
        }
        int r = jeu_action(J, 2, 0, &RJ, suppr);
        if(i != 0){
            mu_assert("jeu_mouvement (d) : err1", J->prochaines_pce->dernier == nouv_a_sortir);
        }
        else{
            mu_assert("jeu_mouvement (d) : err1", J->prochaines_pce->dernier->piece == &RJ);
        }
        mu_assert("jeu_mouvement (d) : err2", r &&J->prochaines_pce->premier->piece == &RJ);
        mu_assert("jeu_mouvement (d) : err3", r && J->terr->liste[GENERALE]->premier->piece == t[i]);
        jeu_libere(J);
    }
    
///////////////////////////////////////////////////////
/// FAIRE TEST ROTATION  + SCORE /////////////////////
///                              /////////////////////
//////////////////////////////////////////////////////

    return 0;
}


static char *all_tests(){      
    mu_run_test(test_creer_jeu);
    mu_run_test(test_jeu_action);
    // mu_run_test(test_piece_valide);
    // mu_run_test(test_piece_compare);
    // mu_run_test(test_piece_affiche);
    return 0;
}

int tests_jeu()
{
    char *result = all_tests();
    printf("--tests piece.c: ");

    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("\t\tTests run: %d\n", tests_run);

    return result == 0;
}
