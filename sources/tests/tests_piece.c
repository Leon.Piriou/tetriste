/* Source :  <http://www.jera.com/techinfo/jtns/jtn002.html> */

#include "./tests/tests_piece.h"


static int tests_run = 0;
static Piece p1 = {ROUGE, CARRE};
static Piece p2 = {JAUNE, TRIANGLE};
static Piece p3 = {ROUGE, LOSANGE};
static Piece p4 = {VERT, CARRE};
static Piece p5 = {ROUGE, CARRE};



static char *test_cree_piece(){
    Piece *p = cree_piece(ROUGE, CARRE);
    mu_assert("cree_piece : err1", p->coul==ROUGE && p->forme==CARRE);
    mu_assert("cree_piece : err2", sizeof(*p) == sizeof(Piece));
    free(p);

    p = cree_piece(CARRE, ROUGE);
    mu_assert("cree_piece : err3", p == NULL);
    free(p);
    return 0;
}

static char *test_piece_aleatoire(){
    for(int i=0 ; i<1000 ; i++){
        Piece *p = piece_aleatoire();
        mu_assert("piece_aleatoire : err1", p->coul <= 4 && p->forme <= 8
                                            && p->coul >= 1 && p->forme >= 5);
        mu_assert("piece_aleatoire : err2", sizeof(*p) == sizeof(Piece));
        free(p);
    }
    return 0;
}

static char *test_piece_valide(){
    mu_assert("piece_valide : err1", piece_valide(-1, -2) == 0);
    mu_assert("piece_valide : err2", piece_valide(10, 9) == 0);
    mu_assert("piece_valide : err3", piece_valide(ROUGE, LOSANGE) == 1);
    mu_assert("piece_valide : err4", piece_valide(BLEU, ROND) == 1);
    return 0;
}

static char *test_piece_compare(){
    mu_assert("piece_compare : err1", piece_compare(&p1, &p2) == 0);
    mu_assert("piece_compare : err2", piece_compare(&p1, &p3) == 1);
    mu_assert("piece_compare : err3", piece_compare(&p1, &p4) == 2);
    mu_assert("piece_compare : err4", piece_compare(&p1, &p5) == 3);
    mu_assert("piece_compare : err6", piece_compare(&p1, &p1) == 4);
    return 0;
}

static char *test_piece_affiche(){
    Piece *p = cree_piece(BLEU, LOSANGE);
    printf("\t--Verification visuelle :\n");

    printf("\tLosange Bleu : ");
    piece_affiche(p) ; free(p);
    printf("\n");

    printf("\tCarre Rouge : ");
    piece_affiche(&p1);
    printf("\n");

    printf("\tTriangle Jaune : ");
    piece_affiche(&p2);
    printf("\n");    

    p = cree_piece(VERT, ROND);
    printf("\tRond Vert : ");
    piece_affiche(p) ; free(p);
    printf("\n");

    p = NULL;
    printf("\tPiece NULL : ");
    piece_affiche(p);
    printf("\n");

    Piece p6 = {ROND, VERT};
    printf("\tPiece invalide : ");
    piece_affiche(&p6);
    printf("\n");


    return 0;
}

static char *all_tests(){      
    mu_run_test(test_cree_piece);
    mu_run_test(test_piece_aleatoire);
    mu_run_test(test_piece_valide);
    mu_run_test(test_piece_compare);
    mu_run_test(test_piece_affiche);
    return 0;
}

int tests_piece()
{
    char *result = all_tests();
    printf("--tests piece.c: ");

    if (result != 0) {
        printf("%s\n", result);
    }
    else {
        printf("ALL TESTS PASSED\n");
    }
    printf("\t\tTests run: %d\n", tests_run);

    return result == 0;
}
