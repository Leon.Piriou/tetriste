// Affichage du jeu avec SDL
/* Source du tuto pour SDL : https://lazyfoo.net/tutorials/SDL/ et tests inclus dans la SDL 
* et documentation : https://wiki.libsdl.org/SDL2/
*/

#include "aff_sdl.h"

/// @brief Charge toutes les images des pieces correspondant au tableau ttes_pieces de tetriste.c
int charge_img_pces(SDL_Surface **img_pces){
    char *nom_fichier[N_COUL*N_FORME] = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15"};
    char *fichier = NULL;
    for(int i=0 ; i<N_COUL*N_FORME ; i++){
        fichier = concat_cheminVersFichier(CHEMIN_IMG, nom_fichier[i], ".bmp");
        if(! fichier){
            printf("-- %s : probleme creation nom du fichier complet\n", __func__);
            return 0;
        }
        img_pces[i] = SDL_LoadBMP(fichier);
        free(fichier);
        if( ! img_pces[i]){
            printf("-- %s : prblm chargement img (%s)\n", __func__, SDL_GetError());
            return 0;
        }
    }
    return 1;
}

void libere_img_pces(SDL_Surface **img_pces){
    for(int i=0 ; i<N_COUL*N_FORME ; i++){
        SDL_FreeSurface(img_pces[i]);
    }
}

int genere_emplacements_pces(SDL_Window *pge, SDL_Rect **emplacements, int n, int hauteur){
    int largeur, pos = 0;
    SDL_GetWindowSize(pge, &largeur, NULL);
    largeur /= MAX_PCES;
    for(int i=0 ; i<n ; i++){
        emplacements[i] = malloc(sizeof(SDL_Rect));
        if(emplacements[i] == NULL) return 0;
        emplacements[i]->x = pos;
        emplacements[i]->y = hauteur;
        emplacements[i]->w = largeur;
        emplacements[i]->h = largeur;
        pos += largeur;
    }
    return 1;
}

void libere_emplacements_pces(SDL_Rect **emplacements, int n){
    for(int i=0 ; i<n ; i++){
        free(emplacements[i]);
    }
}

/// @brief Initialise la SDL, la page principale et le contenu principal de la page.
/// @param pge Pointeur sur le pointeur de la page principale
/// @param contenu Pointeur sur le pointeur du contenu (surface) principal
/// @return 0 en cas d'echec (et ferme la SDL et la memoire allouee) ; 1 si succes
int aff_sdl_init(SDL_Window **pge, SDL_Surface **contenu){
    *pge = NULL;
    *contenu = NULL;
    int r;

    if(SDL_Init(SDL_INIT_VIDEO) < 0){
        quitter("Echec initialisation SDL", 0);
    }
    *pge = SDL_CreateWindow("Tetriste v2.0",SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, LARGEUR, HAUTEUR, SDL_WINDOW_SHOWN);
    if(*pge == NULL){
        quitter("Echec initialisation page principale", 0);
    }
    *contenu = SDL_GetWindowSurface(*pge);
    if(*contenu == NULL){
        SDL_DestroyWindow(*pge);
        quitter("Echec creation surface principale", 0);
    }
    r = SDL_FillRect(*contenu, NULL, SDL_MapRGB((*contenu)->format, 0xFF, 0xFF, 0xFF));
    if(r < 0){
        SDL_DestroyWindow(*pge);
        quitter("Erreur remplissage du contenu principal", 0);
    }
    SDL_Delay(1000);
    SDL_FillRect(contenu_ppal, NULL, SDL_MapRGB(contenu_ppal->format, 255, 255, 255));
    return 1;
}

int aff_sdl_bienvenue(SDL_Window *pge, SDL_Surface *contenu, SDL_Surface **img){
    *img = NULL;
    char *chemin_bmp = concat_cheminVersFichier(CHEMIN_IMG, IMG_BVNUE, "");
    if( ! chemin_bmp){
        printf("-- %s : Erreur concatenation chemin vers fichier\n", __func__);
        return 0;
    }

    *img = SDL_LoadBMP(chemin_bmp);
    free(chemin_bmp);
    if(*img == NULL){
        printf("-- %s : Impossible d'ouvrir l'image %s (%s)\n", __func__, IMG_BVNUE, SDL_GetError());
        return 0;
    }
    if(SDL_BlitScaled(*img, NULL, contenu, NULL) < 0){
        printf("-- %s : Echec affichage de l'image %s (%s)\n", __func__, IMG_BVNUE, SDL_GetError());
        return 0;
    }
    if(SDL_UpdateWindowSurface(pge) < 0){
        printf("-- %s : Echec de la mise a jour de la page (%s)\n", __func__, SDL_GetError());
        return 0;
    }
    #ifdef DEBUG_AFF_SDL
        printf("-- %s : Fin de l'affichage de la bienvenue.\n", __func__);
    #endif
    
    return 1;
}

int aff_regles(void *arg){
    return 0;
}


int aff_sdl_liste(Liste *l, SDL_Surface *contenu_ppal, SDL_Surface **img_pces, SDL_Rect **emplacement){
    Maillon *m = l->premier;
    int indice = -1;
    for(int i=0 ; i<l->taille ; i++){
        indice = piece_encode(m->piece);
        if(SDL_BlitScaled(img_pces[indice], NULL, contenu_ppal, emplacement[i]) < 0){
            printf("-- %s : Echec affichage de l'image %d (%s)\n", __func__, i, SDL_GetError());
            return 0;
        }
    }
    return 1;
}

int aff_sdl_page_principale(){
    SDL_Window *pge_ppale = NULL;
    SDL_Surface *contenu_ppal = NULL;
    SDL_Surface *img_bienvenue = NULL;
    
    SDL_Surface *img_pieces[N_COUL*N_FORME] = {NULL};
    SDL_Rect *emplacements_pces_jeu[MAX_PCES] = {NULL};
    SDL_Rect *emplacements_pces_attente[N_PCES_ATTENTE] = {NULL};

    if( ! aff_sdl_init(& pge_ppale, &contenu_ppal)){
        printf("ERREUR INIT SDL\n");
        return 0;
    }

    aff_sdl_bienvenue(pge_ppale, contenu_ppal, &img_bienvenue);
    #ifdef DEBUG_AFF_SDL
        printf("-- %s : Fin de l'affichage de bienvenue.\n", __func__);
    #endif
    charge_img_pces(img_pieces);
    #ifdef DEBUG_AFF_SDL
        printf("-- %s : Chargement des pieces effectue.\n", __func__);
    #endif
    genere_emplacements_pces(pge_ppale, emplacements_pces_jeu, MAX_PCES, contenu_ppal->h/2);
    genere_emplacements_pces(pge_ppale, emplacements_pces_attente, N_PCES_ATTENTE, 0);
    #ifdef DEBUG_AFF_SDL
        printf("-- %s : Generation des rectangles pour pieces effectuee.\n", __func__);
    #endif
    Piece *p = cree_piece(ROUGE, CARRE);
    Liste *l = liste_init();
    liste_ajout_debut(l, p);
    liste_ajout_debut(l, p);
    liste_ajout_debut(l, p);
    liste_ajout_debut(l, p);
    liste_ajout_debut(l, p);

    aff_sdl_liste(l, contenu_ppal, img_pieces, emplacements_pces_jeu);
    SDL_UpdateWindowSurface(pge_ppale);
    liste_libere(l);
    free(p);

    // a retirer car ne sait pas l'expliquer
    SDL_Event e; int quit = 0; while( quit == 0 ){ while( SDL_PollEvent( &e ) ){ if( e.type == SDL_QUIT ) quit = 1; }}
    SDL_DestroyWindow(pge_ppale);
    
    SDL_FreeSurface(img_bienvenue);
    libere_img_pces(img_pieces);
    libere_emplacements_pces(emplacements_pces_jeu, MAX_PCES);
    libere_emplacements_pces(emplacements_pces_attente, N_PCES_ATTENTE);

    quitter("Execution OK", 1);
}

int aff_sdl_partie(){
    SDL_Event action;
    while (SDL_PollEvent(&action)){
        if(action.type == SDL_KEYDOWN){
            switch (action.key.keysym.sym)
            {
            case SDLK_LEFT:
                // action ajout gauche
                break;
            case SDLK_RIGHT:
                // action ajout droite
            case SDLK_SPACE:
                // rotation
            default:
                break;
            }
        }

    }
    return 1;
}
//     return 1;
// }
// il faut :
//      - affichage des regles
//      - affichage d'une partie (bouton pour rotation / d/ g/ quitter)
//      - recuperer un pseudo
//      - recuperer la commande
//      - une fonction question pour savoir si on ouvre l'ancienne partie