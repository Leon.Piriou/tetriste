#include"sauvegarde.h"


/// @brief Concatene le chemin, le fichier et l'extension. Creer une zone memoire pour stocker la concatenation
/// @param chemin le chemin sans '/' a la fin (rajoute par la fonction)
/// @param fichier nom du fichier sans extension
/// @param ext l'extension du fichier (avec le '.' car pas ajoute par la fonction)
/// @note complexite : Constant
/// @return une zone de char contenant la concatenation de ces trrois arguments
char *concat_cheminVersFichier(char *chemin, char *fichier, char *ext){
    char *chemin_fichier = malloc(2+strlen(chemin)+strlen(fichier)+strlen(ext));  // pas oublier le '/' et '\0'
    if(chemin_fichier == NULL){
        printf("Erreur creation chemin relatif vers partie\n");
        return 0;
    }
    sprintf(chemin_fichier, "%s/%s%s", chemin, fichier, ext);
    #ifdef DEBUG_SAUVEGARDE
        printf("-- %s : fichier a ouvrir : %s\n", __func__, chemin_fichier);
    #endif
    return chemin_fichier;
}

/// @brief Enregistre la partie dans un ficher nomme par le pseudo.
/// @param jeu le jeu a sauvegarder
/// @param fichierdelapartie le nom sous lequel on enregistre
/// @note complexite : lineaire (taille de la liste generale)
/// @return 0 echec; 1 succes
int sauvegarde_jeu(Jeu* jeu, char *fichierdelapartie)
{
    int taille = jeu->terr->liste[GENERALE]->taille;
    char *chemin_fichier = concat_cheminVersFichier(CHEMIN_PARTIES, fichierdelapartie, "");
    if( ! chemin_fichier){
        return 0;
    }

    FILE *sauvegarde=fopen(chemin_fichier, "w");
    free(chemin_fichier);
    if(sauvegarde==NULL)
    {
        printf("-- %s : erreur d'ouverture \n", __func__);
        return 0;
    }

    fprintf(sauvegarde,"TETRISTE\n%d\n%d\n",jeu->score,taille);

    Maillon *maillon=jeu->prochaines_pce->premier;
    for (int i=0;i<N_PCES_ATTENTE;i++)
    {
        fprintf(sauvegarde,"%d ",piece_encode(maillon->piece));
        maillon=maillon->suiv;
    }
    fprintf(sauvegarde,"\n");
    maillon=jeu->terr->liste[GENERALE]->premier;
    for(int i=0;i<taille;i++){
        fprintf(sauvegarde,"%d ",piece_encode(maillon->piece));
        maillon=maillon->suiv;
    }
    fprintf(sauvegarde,"\n");

    fclose(sauvegarde);
    return 1;
}

/// @brief Charge un jeu depuis un fichier nomme pseudo
/// @param jeu le jeu dans lequel on ecrit les infos qu'on lit
/// @param pseudo le nom du fichier a ouvrir
/// @param ttes_pces Toutes les pieces pour decoder le fichier.
/// @note complexite : lineaire (taille de la liste generale)
/// @return 0 echec; 1 succes
int chargement_jeu(Jeu *jeu, char * pseudo, Piece **ttes_pces)
{
    char *chemin_fichier = concat_cheminVersFichier(CHEMIN_PARTIES, pseudo, "");
    if( ! chemin_fichier){
        return 0;
    }

    FILE *fjeu=fopen(chemin_fichier,"r");
    free(chemin_fichier);
    if(fjeu == NULL){
        printf("-- %s : Echec ouverture de la partie de %s\n", __func__, pseudo);
        return 0;
    }

    int taille_terr = 0;
    int r = fscanf(fjeu,"TETRISTE\n%d\n%d\n", &(jeu->score), &taille_terr);
    if(r != 2){
        printf("-- %s : Erreur lecture fichier de %s", __func__, pseudo);
        fclose(fjeu);
        return 0;
    }

    int code_pce = -1, err = 0;
    for(int i=0 ; i<N_PCES_ATTENTE ; i++){
        if(fscanf(fjeu, "%d ", &code_pce) != 1){
            err = 1;
            break;
        }
        if( ! liste_ajout_fin(jeu->prochaines_pce, ttes_pces[code_pce])){
            err = 1;
            break;
        }
    }
    if(err){
        printf("-- %s : Erreur chargement liste d'attente\n", __func__);
        fclose(fjeu);
        return 0;
    }

    code_pce = -1, err = 0;
    for(int i=0 ; i<taille_terr ; i++){
        if(fscanf(fjeu, "%d ", &code_pce) != 1){
            err = 1;
            break;
        }
        if( ! terrain_ajoute_droite(jeu->terr, ttes_pces[code_pce])){
            err = 1;
            break;
        }
    }
    if(err){
        printf("-- %s : Erreur chargement liste d'attente\n", __func__);
        fclose(fjeu);
        return 0;
    }
    fclose(fjeu);
    return 1;
}
