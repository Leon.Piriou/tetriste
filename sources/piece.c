#include "piece.h"


/// @brief Creation d'une piece de couleur 'coul' et de forme 'forme'
/// @param coul Valeur codant la couleur (entre 1 et 4)
/// @param forme Valeur codant la forme (entre 5 et 8)
/// @note complexite : constant
/// @return NULL si echec (malloc ou mauvaise valeur 'coul' ou 'forme'). Un pointeur sur la piece cree sinon
Piece *cree_piece(Couleur coul, Forme forme){
    if(piece_valide(coul, forme)){
        Piece *p = malloc(sizeof(Piece));
        if(!p){ return NULL; }

        p->coul = coul;
        p->forme = forme;

        return p;
    }
    return NULL;
}

/// @brief Creation d'une piece a la couleur et la forme aleatoire
/// @note complexite : constant
/// @return Un pointeur sur une piece aleatoire. NULL si echec
Piece *piece_aleatoire(){
    Couleur coul = (rand() % 4) + 1;
    Forme forme = (rand() % 4) + 5;
    
    return cree_piece(coul, forme);
}

/// @brief Verifie validite de la valeur de la couleur et de la forme (pour creation d'une piece)
/// @note complexite : constant
/// @return 1 si valide, 0 sinon
int piece_valide(Couleur coul, Forme forme){
    return ((ROUGE <= coul && coul <= BLEU) && 
            (LOSANGE <= forme && forme <= ROND));
}

/// @brief Compare deux pieces selon leur couleur, leur forme et leur adresse memoire
/// @param p1 Pointeur sur la premiere piece a comparer
/// @param p2 Pointeur sur la deuxieme piece a comparer
/// @note complexite : constant
/// @return 0:differentes ; 1:meme couleur ; 2:meme forme ; 3:identiques ; 4:meme adresse memoire
int piece_compare(Piece *p1, Piece *p2){
    // Cas meme pointeurs, rien d'autre a verifier
    if(p1 == p2){
        return 4;
    }
    int r = 0;
    if(p1->forme == p2->forme){
        r |= 2;
    }
    if(p1->coul == p2->coul){
        r |= 1;
    }
    return r;
}

/// @brief Affichage d'une piece en couleur avec un espace final
/// @param p Pointeur sur la premiere piece a afficher
/// @note complexite : constant
/// @return 1 si affichage reussi, 0 sinon
int piece_affiche(Piece *p){
    if(p && piece_valide(p->coul, p->forme)){
        char forme = '?';
        switch(p->forme){
            case LOSANGE:
                forme = 'L';
                break;
            case CARRE:
                forme = 'C';
                break;
            case TRIANGLE:
                forme = 'T';
                break;
            case ROND:
                forme = 'R';
                break;
            default:
                printf("Erreur de forme dans 'piece_affiche' (impossible)\n");
                return 0;
        }
        printf("\033[1;3%dm%c\033[0m", p->coul, forme);
        return 1;
    }
    printf("\033[1;5m?\033[0m");
    return 0;
}

/// @brief Renvoie la place de la piece dans le tableau contenant toutes les pieces (cf tetriste.c)
/// @note complexite : constant
/// @param piece Piece que l'oin veut encoder
int piece_encode(Piece *piece)
{
    return (piece->coul - 1)*N_COUL + (piece->forme - 1 - N_FORME);
}