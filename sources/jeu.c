#include "jeu.h"

static int jeu_mouvement(Jeu *J, Piece *nvelle, int (*mvmt)(Terrain *T, Piece *p));


// =============== FONCTIONS STATIQUES ==================================================================================================
// ! ne verifient pas validite de leurs arguments car c'est fait dans les fonctions appelants ces fonctions statiques !
//

/// @brief Ajoute la prochaine piece a sortir dans la liste d'attente a droite ou a gauche dans le terrain de jeu et rajoute une nouvelle piece dans la liste d'attente
/// @param J le jeu dans lequel on rajoute une piece
/// @param nvelle piece a rajouter dans la liste d'attente
/// @param mvmt fonction effectuant un ajout a droite ou a gauche dans le terrain de jeu
/// @note complexite : constant
/// @return 1 si le mouvement a bien ete fait. -1 si rien ne s'est passe
static int jeu_mouvement(Jeu *J, Piece *nvelle, int (*mvmt)(Terrain *T, Piece *p)){
    Maillon *m = liste_extraire_fin(J->prochaines_pce);
    if(m == NULL){
        #ifdef DEBUG_JEU
            printf("-- jeu_mouvement : prochaines_pce semble vide car on a m = %p\n", m);
	    #endif
        return -1;
    }
    int r = mvmt(J->terr, m->piece);
    if((r == 1) && (liste_ajout_debut(J->prochaines_pce, nvelle))){ // Mouvement fait et on reussit a rajouter la nouvelle piece dans la liste d'attente
        #ifdef DEBUG_JEU
            printf("-- jeu_mouvement : mouvement effectue, nvelle piece rajoute liste d'attente\n");
        #endif
    }
    else{ // Echec du mouvement : il y a eu un gros bug, inutile de remettre l'ancienne piece a sa place dans la file d'attente
        // liste_ajout_fin(J->prochaines_pce, m->piece); // intutile bug trop gros
        #ifdef DEBUG_JEU
            printf("-- jeu_mouvement : echec mouvement , on remet l'ancienne piece a sa place\n");
        #endif
    }
    free(m);
    return r;
}
// ===========================================================================================================================================



/// @brief Cree une structure Jeu, avec l'allocation memoire pour ses differentes composantes.
/// @note complexite : lineaire
/// @return Un pointeur sur une struct jeu, ou NULL si echec d'une des allocations (le reste des allocations reussie est dans ce cas liberee)
Jeu *creer_jeu(){
    Jeu *J = malloc(sizeof(Jeu));
    if(J){
        J->score = 0;
        if((J->prochaines_pce = liste_init())){
            if((J->terr = creer_terrain())){
                #ifdef DEBUG_JEU
                    printf("-- creer_jeu : creation jeu %p\n", J);
                #endif
                return J;
            }
            free(J->prochaines_pce);
        }
        free(J);
    }
    #ifdef DEBUG_JEU
        printf("-- creer_jeu : echec allocation mem\n");
    #endif
    return NULL;
}

/// @brief Libere la memoire d'une strcut jeu
/// @note complexite : lineaire (nbre de piece)
/// @param J le Jeu a liberer
void jeu_libere(Jeu *J){
    liste_libere(J->prochaines_pce);
    terrain_libere(J->terr);
    free(J);
    #ifdef DEBUG_JEU
        printf("-- jeu_libere : liberation mem jeu %p\n", J);
    #endif
}

/// @brief Affichage d'un jeu avec ses differentes valeurs
/// @note complexite : lineaire (nbre de piece)
/// @param J pointeur sur le Jeu a afficher
void jeu_affiche(Jeu *J){
    printf("Jeu %p.\nScore:%d\nProchaines pieces : ", J, J->score);
    liste_affiche(J->prochaines_pce);
    affiche_terrain(J->terr);
}

/// @brief Calcul de score.
/// @param tab_s Contient le nombre d'elements supprimes de plusieurs combo. En premiere case le type de mouvement effectue pour atteindre cet etat du jeu.
/// @param n Nombre de combo, soit le nombre de case non nulle - 1 dans tab_s
/// @note complexite : lineaire (nbre de suprresions)
void score(Jeu *J, int tab_s[], int n){
    #ifdef DEBUG_JEU
        printf("-- score() : n = %d\n", n);
    #endif
    int S = 0;
    for(int i=1 ; i<n+1 ; i++){   // le premier elem du tableau est le type de mouvement fait pour obtenir ce score
        #ifdef DEBUG_JEU
            printf("-- score() : tab_s[%d] = %d\n", i, tab_s[i]);
        #endif
        int coef_mul_diff = tab_s[i] - MIN_ALIGNEMENT + 1;  // En cas d'alignement de plus de trois piece,
        S += tab_s[i] * coef_mul_diff;          // on multiplie par la difference au nombre min de piece.
    }
    S *= n;     // multiplication du score obtenu par le nombre de combo
    S *= (1*tab_s[0] + 1); // double le score en cas de rotation
    #ifdef DEBUG_JEU
        printf("-- score() : S = %d\n", S);
    #endif
    J->score += S;
}


/// @brief Execute une action (rotation, ajout droite/gauche), cherche les alignements a supprimer, calcule le score.
/// @param J Jeu dans lequel on execute l'action
/// @param cmd Commande a executer 1:ajoutDroite 2:ajoutGauche 3:rotation
/// @param param Pour la rotation, indique quel type de rotation (ROUGE,...,ROND)
/// @param nvelle piece a ajouter a la file d'attente en cas d'ajout de piece, au terrain de jeu
/// @param suppr Tableau stockant les suppressions.
/// @note complexite : lineaire (nbre de piece) dans le pire cas
/// @return -1 en cas d'echec. 0 si aucun mouvement n'a ete fait. 1 si un mouvement a ete fait et le score actualise
int jeu_action(Jeu *J, int cmd, int param, Piece *nvelle, int *suppr){
    #ifdef DEBUG_JEU
        printf("-- jeu_action : J:%p\tcmd=%d\tparam=%d\n", J, cmd, param);
    #endif
    int r = 0;
    switch (cmd){
        case 3:
            r = terrain_rotation(J->terr, param);
            suppr[0] = ROTATION;
            #ifdef DEBUG_JEU
                printf("-- jeu_action : rotation (r=%d)\n", r);
            #endif
            break;
        case 2:
            r = jeu_mouvement(J, nvelle, terrain_ajoute_gauche);
            suppr[0] = PLACE_PCE;
            #ifdef DEBUG_JEU
                printf("-- jeu_action : ajout gauche (r=%d)\n", r);
            #endif
            break;
        case 1:
            r = jeu_mouvement(J, nvelle, terrain_ajoute_droite);
            suppr[0] = PLACE_PCE;
            #ifdef DEBUG_JEU
                printf("-- jeu_action : ajout droite (r=%d)\n", r);
            #endif
            break;
        default:
            #ifdef DEBUG_JEU
                printf("-- jeu_action : cmd(%d) est non valide (r=%d).\n", cmd, r);
            #endif
            break;
    }
    if((r == 1) && (J->terr->liste[GENERALE]->taille >= MIN_ALIGNEMENT)){ // On calcule le score s'il y a plus de MIN_ALIGNEMENT elements.
        #ifdef DEBUG_JEU
            printf("-- jeu_action : mouvement fait. Ancien score :%d\n", J->score);
        #endif
        int nbre_suppr = terrain_suppr_repet(J->terr, suppr);
        score(J, suppr, nbre_suppr);
        #ifdef DEBUG_JEU
            printf("-- jeu_action : Nouveau score:%d\n", J->score);
        #endif
    }
    #ifdef DEBUG_JEU
	    printf("-- jeu_action : pas de mouvement (r=%d) ou jeu trop petit (%d pieces en jeu)\n", 
		r, J->terr->liste[GENERALE]->taille);
    #endif
    return r == 1;
}
