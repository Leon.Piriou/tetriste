# Tetriste

## Statut de ce projet
En cours, reste la partie graphique à faire, ainsi qu'une IA de meilleur coup.


## Description
Jeu de logique sur terminal (version graphique en cours). Sauvegarde de la partie et plusieurs joueurs possibles (non simultanément).

## Installation (pour Ubuntu)
Nécessite un compilateur (ex: gcc).
Télécharger l'archive et l'extraire dans l'emplacement de votre choix.
Ouvrez un terminal et placez-vous dans le répertoire `tetriste` créé par l'extraction. Compiler le projet avec `make`. 
Enfin retirer les fichiers objets (`*.o`). Cela donne :
> cd /chemin/vers/tetriste <br>
> make <br>
> make nettoie <br>

Vous avez normalement un nouveau fichier binaire `tetriste` contenant le jeu.

## Usage
Une fois le projet compilé, vous pouvez le lancer avec :

> ./tetriste -t

L'option `-t` permet de jouer dans le terminal. Sans option, cela lance le mode graphique (pas encore prêt)

## Aide
En cas de problème, vous pouvez contacter <vincent.fablet@dauphine.eu> ou <leon.piriou@dauphine.eu>. 
Vous pouvez également consulter le fichier `dev.pdf`, contenu dans le projet pour essayer de trouver la source de votre problème.

## Implémentation future
 * Un mode de jeu graphique.
 * Des niveaux de difficultés variés.
 * Une IA pour conseiller le meilleur coup.
 * Un fichier contenant une signature de chaque fichier (hachage) pour limiter les tentatives de modification artificielle d'une partie.
 * Une base de donnée en ligne contenant le meilleur score de tous les joueurs ayant installé le jeu.
 * Un minuteur limitant le temps de réflexion pour chaque coup.


## Sources extérieures
 * Bibliothèque [SDL2](https://libsdl.org).
 * Tutoriels SDL2 : [developpez.com](https://alexandre-laurent.developpez.com/tutoriels/sdl-2/) / [lazyfoo.net](https://lazyfoo.net/tutorials/SDL/index.php)
 * Modification du [fichier test du tp9](https://www.lamsade.dauphine.fr/~sikora/ens/c/tp9-tests.c) dans la partie des tests.
 * Mini-bibliothèque [MinUnit](http://www.jera.com/techinfo/jtns/jtn002.html) pour les tests.
 * Illustrations : [patorjk.com](http://www.patorjk.com/software/taag/#p=testall&h=1&v=1&c=c&f=Blocks&t=Tetriste)

## Modifier le projet
Si vous souhaitez modifier ce projet, vous pouvez lire `dev.pdf` pour connaître plus en détail l'implémentation que nous avons choisie et modifier ce qui vous semble utile.

Pour exécuter les tests, vous pouvez compiler le projet avec :

> make tests

## Remerciement
Nous remercions E. Lazard pour son cours de C qui nous a permi de mettre en oeuvre ce projet, et également M. Bounaime et F. Sikora pour leur aide.

## License
Faites absolument ce que vous voulez de ce projet.


