# Makefile pour compiler Tetriste

tete = ./en_tetes
src = ./sources

CFLAGS = -Wall -g -iquote $(tete) -B ./bibli -fsanitize=address

objets = affichage_ascii.o jeu.o joueur.o liste_double.o \
		 maillon.o main.o piece.o sauvegarde.o terrain.o tetriste.o

obj_tests = tests_jeu.o tests_liste_double.o tests_piece.o \
			tests_principaux.o tests_terrain.o

all: tetriste

tetriste: $(objets) makefile
	gcc $(CFLAGS) -o tetriste $(objets) -lSDL2

tests: $(objets) $(obj_tests) makefile
	gcc $(CFLAGS) -c $(src)/main.c -o main.o -DTESTS
	gcc $(CFLAGS) -o tetriste_tests $(objets) $(obj_tests) -DTESTS -lSDL2
	rm main.o

efface: nettoie
	rm -f ./tetriste ./tetriste_tests 2> /dev/null

nettoie:
	rm -f $(objets) $(obj_tests) 2> /dev/null

## Compilation Fichiers objets :

# aff_sdl.o: $(src)/aff_sdl.c $(tete)/aff_sdl.h
# 	gcc $(CFLAGS) -c $(src)/aff_sdl.c -o aff_sdl.o
#

affichage_ascii.o: $(src)/affichage_ascii.c $(tete)/affichage_ascii.h \
					$(tete)/jeu.h
	gcc $(CFLAGS) -c $(src)/affichage_ascii.c -o affichage_ascii.o

jeu.o: $(src)/jeu.c $(tete)/jeu.h \
		$(tete)/terrain.h
	gcc $(CFLAGS) -c $(src)/jeu.c -o jeu.o

joueur.o: $(src)/joueur.c $(tete)/joueur.h \
		$(tete)/affichage_ascii.h
	gcc $(CFLAGS) -c $(src)/joueur.c -o joueur.o


liste_double.o: $(src)/liste_double.c $(tete)/liste_double.h \
				$(tete)/piece.h $(tete)/maillon.h
	gcc $(CFLAGS) -c $(src)/liste_double.c -o liste_double.o

maillon.o: $(src)/maillon.c $(tete)/maillon.h \
			$(tete)/piece.h
	gcc $(CFLAGS) -c $(src)/maillon.c -o maillon.o

main.o: $(src)/main.c $(tete)/main.h \
		$(tete)/tetriste.h $(tete)/tests/tests_principaux.h
	gcc $(CFLAGS) -c $(src)/main.c -o main.o

piece.o: $(src)/piece.c $(tete)/piece.h
	gcc $(CFLAGS) -c $(src)/piece.c -o piece.o

sauvegarde.o: $(src)/sauvegarde.c $(tete)/sauvegarde.h
	gcc $(CFLAGS) -c $(src)/sauvegarde.c -o sauvegarde.o

terrain.o: $(src)/terrain.c $(tete)/terrain.h \
			$(tete)/piece.h $(tete)/maillon.h $(tete)/liste_double.h
	gcc $(CFLAGS) -c $(src)/terrain.c -o terrain.o

tetriste.o: $(src)/tetriste.c $(tete)/tetriste.h \
			$(tete)/affichage_ascii.h $(tete)/jeu.h $(tete)/terrain.h
	gcc $(CFLAGS) -c $(src)/tetriste.c -o tetriste.o


# Compilation des tests

tests_jeu.o: $(src)/tests/tests_jeu.c $(tete)/tests/tests_jeu.h \
			$(tete)/jeu.h
	gcc $(CFLAGS) -c $(src)/tests/tests_jeu.c -o tests_jeu.o

tests_liste_double.o: $(src)/tests/tests_liste_double.c $(tete)/tests/tests_liste_double.h \
					$(tete)/liste_double.h
	gcc $(CFLAGS) -c $(src)/tests/tests_liste_double.c -o tests_liste_double.o

tests_piece.o: $(src)/tests/tests_piece.c $(tete)/tests/tests_piece.h \
				$(tete)/piece.h
	gcc $(CFLAGS) -c $(src)/tests/tests_piece.c -o tests_piece.o

tests_principaux.o: $(src)/tests/tests_principaux.c $(tete)/tests/tests_principaux.h \
					$(tete)/tests/tests_jeu.h $(tete)/tests/tests_liste_double.h $(tete)/tests/tests_piece.h $(tete)/tests/tests_terrain.h
	gcc $(CFLAGS) -c $(src)/tests/tests_principaux.c -o tests_principaux.o

tests_terrain.o: $(src)/tests/tests_terrain.c $(tete)/tests/tests_terrain.h \
				$(tete)/piece.h $(tete)/liste_double.h $(tete)/terrain.h
	gcc $(CFLAGS) -c $(src)/tests/tests_terrain.c -o tests_terrain.o
