#ifndef TESTS_TERRAIN_H
    #define TESTS_TERRAIN_H

    #include <stdlib.h>
    #include <string.h>
    #include "debug.h"
    #include "tests/tests_principaux.h"
    #include "liste_double.h"
    #include "piece.h"
    #include "terrain.h"
    
    int tests_terrain();

#endif