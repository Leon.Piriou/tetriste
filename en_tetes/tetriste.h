#ifndef TETRISTE_H
    #define TETRISTE_H

    #define MAX_PCES 15

    #include <stdlib.h>
    #include <stdio.h>
    #include "affichage_ascii.h"
    #include "jeu.h"
    #include "terrain.h"
    #include"sauvegarde.h"
    #include "joueur.h"

    typedef struct fct_EntreeSortie{
        int (*aff_bienvenue)(void *arg);
        void *argBienvenue;

        int (*recup_pseudo)(char *pseudo, void *arg);
        void *argRecupPseudo;

        int (*question)(char *phrase, void *arg);
        void * argQuestion;

        int (*aff_regles)(void *arg);
        void *argRegles;

        int (*aff_partie)(Jeu *jeu, void *arg);
        void *argJeu;

        int (*recup_cmd)(int *cmd, int *param, void *arg);
        void* argRecupCmd;
    } Fct_EntreeSortie;

    int ttste_creer_ttes_pces(Piece **ttes_pces, int n_coul, int n_forme);
    int jeu_init_liste_attente(Jeu *J, Piece **tab, int n);
    int tetriste_principal(Fct_EntreeSortie *fes);


#endif