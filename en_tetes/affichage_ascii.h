#ifndef AFFICHAGE_ASCII_H
    #define AFFICHAGE_ASCII_H

    #define MAX_PSEUDO 100
    #define NBRE_CMDS 5
    
    
    #include <stdio.h>
    #include <ctype.h>
    #include <string.h>
    #include <unistd.h>
    #include "jeu.h"

    int term_aff_bienvenue(void* nul);
    int term_aff_regles(void *nul);
    int term_recup_pseudo(char *pseudo, void *nul);
    int term_question(char *char_question, void *nul);
    int term_aff_partie(Jeu *J, void *nul);
    int term_recup_cmd(int *cmd, int *param, void *nul);
    // int term_execute_coup(Jeu *jeu, Piece **ttes_pces, int *suppr, void *nul);


#endif
