#ifndef MAILLON_H
    #define MAILLON_H

    #include <stdio.h>
    #include <stdlib.h>
    #include "piece.h"

    typedef struct maillon{
        Piece *piece;
        struct maillon *prec;
        struct maillon *suiv;
    } Maillon;
    
    Maillon *creer_maillon(Piece *p);

#endif

