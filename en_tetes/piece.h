#ifndef PIECE_H
    #define PIECE_H

    #define N_COUL 4
    #define N_FORME 4
    
    #include <stdio.h>
    #include <stdlib.h>

    typedef enum couleur {ROUGE=1, VERT, JAUNE, BLEU} Couleur;
    typedef enum forme {LOSANGE=5, CARRE, TRIANGLE, ROND} Forme;

    typedef struct piece 
    {
        Couleur coul;
        Forme forme;
    } Piece;

    Piece *cree_piece(Couleur coul, Forme forme);
    Piece *piece_aleatoire(void);

    int piece_valide(Couleur coul, Forme forme);
    int piece_compare(Piece *p1, Piece *p2);

    int piece_affiche(Piece *p);
    int piece_encode(Piece *piece);

#endif