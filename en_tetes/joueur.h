#ifndef JOUEUR_H
    #define JOUEUR_H

    #define F_CLASSEMENT "classement.txt"
    // #define N_CLASSEMENT 10
    #define F_PREMIERS "les_10_premiers.txt"

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include "affichage_ascii.h"

    typedef struct joueur
    {
        int score;
        char pseudo[MAX_PSEUDO];
    } Joueur;

    void libere_tab_joueur(Joueur **tab, int n);
    Joueur **charge_classement(char *pseudo, int *n_joueurs, int *place, int *est_nvea);
    int joueur_sauvegarde_classement(Joueur **tab_j, int n_joueurs);
    int extraction_dix_premier();
    
#endif