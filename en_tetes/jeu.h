#ifndef JEU_H
    #define JEU_H

    #define N_PCES_ATTENTE 5

    #define ROTATION 1
    #define PLACE_PCE 0
    
    #include "debug.h"
    #include "terrain.h"

    typedef struct jeu
    {
        Liste *prochaines_pce;
        Terrain *terr;
        int score;
    } Jeu;

    Jeu *creer_jeu(void);
    void jeu_libere(Jeu *J);
    void jeu_affiche(Jeu *J);
    void score(Jeu *J, int tab_s[], int n);
    int jeu_action(Jeu *J, int cmd, int param, Piece *nvelle, int *suppr);

#endif
