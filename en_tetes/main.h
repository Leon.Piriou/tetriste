#ifndef MAIN_H
    #define MAIN_H

    //#define TESTS
    #include <stdio.h>
    #include <stdlib.h>
    #include <time.h>
    #include "affichage_ascii.h"
    #include "tetriste.h"
    #include "aff_sdl.h"
    #include "debug.h"
    #ifdef TESTS
        #include "tests/tests_principaux.h"
    #endif

    int main();
    
#endif
