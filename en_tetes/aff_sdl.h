#ifndef AFF_SDL
    #define AFF_SDL

    #define LARGEUR 680
    #define HAUTEUR 480
    #define TAILLE_PCE 50
    #define CHEMIN_IMG "./img"
    #define IMG_BVNUE "corneille_a_venir.bmp"

    #define quitter(msg, code) printf("-- %s : %s (%s)\n", __func__, msg, SDL_GetError()); \
                                SDL_Quit(); return code
    #define UNIX
    #ifdef UNIX
        #include "SDL/SDL.h"

    #else
        #include <SDL2/SDL.h>
    #endif
    #include <stdio.h>
    #include <string.h>
    #include "tetriste.h"
    #include "sauvegarde.h"

    int aff_sdl_init(SDL_Window **pge, SDL_Surface **contenu);
    int aff_sdl_page_principale();
#endif
