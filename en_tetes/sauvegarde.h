#ifndef SAUVEGARDE_H
    #define SAUVEGARDE_H

    #define CHEMIN_PARTIES "./.parties"

    #include <stdio.h>
    #include <string.h>
    #include "debug.h"
    #include"jeu.h"

    char *concat_cheminVersFichier(char *chemin, char *fichier, char *ext);
    int sauvegarde_jeu(Jeu* jeu, char*fichierdelapartie);
    int chargement_jeu(Jeu *jeu, char * pseudo, Piece **ttes_pces);
#endif 
