#ifndef LISTE_DOUBLE_H
    #define LISTE_DOUBLE_H
        
    #include <stdlib.h>
    #include <stdio.h>
    #include "debug.h"
    #include "piece.h"
    #include "maillon.h"

    typedef struct {
        Maillon *premier;
        Maillon *dernier;
        int taille;
    } Liste;

    Liste *liste_init();

    int liste_ajout_debut(Liste *l, Piece *p);
    int liste_ajout_fin(Liste *l, Piece *p);

    void liste_affiche(Liste *l);
    // void liste_affiche_inverse(Liste *l);
    // void maillon_affiche_inverse_rec(Maillon *m);
    // void liste_affiche_inverse_rec(Liste *l);

    Maillon *liste_extraire_debut(Liste *l);
    Maillon *liste_extraire_fin(Liste *l);
    void liste_extraire_ssChaine(Liste *l, Maillon *deb, Maillon *fin, int taille_ssCh);
    int liste_trouve_repet(Liste *l, Maillon *actuel, Maillon **fin);


    Maillon *liste_supprimer(Liste *l, Piece *x);

    void liste_tourner_droite(Liste *l);
    void liste_tourner_gauche(Liste *l);

    void liste_libere(Liste *l);
#endif