#ifndef TERRAIN_H
    #define TERRAIN_H

    #define NBRE_COUL_FORM ((N_COUL) + (N_FORME) + 1)
    #define MIN_ALIGNEMENT 3
    #define GENERALE 0

    #include <stdio.h>
    #include <stdlib.h>
    #include "debug.h"
    #include "liste_double.h"
    #include "maillon.h"
    #include "piece.h"
    


    // structure plateau
    typedef struct terrain
    {
        Liste *liste[NBRE_COUL_FORM];
    }Terrain;

    Terrain* creer_terrain();
    void terrain_libere(Terrain *T);
    void affiche_terrain(Terrain *T);
    int terrain_ajoute_droite(Terrain *T, Piece *piece);
    int terrain_ajoute_gauche(Terrain *T, Piece *piece);
    int terrain_rotation(Terrain *T, int type_piece);
    int terrain_suppr_repet(Terrain *T, int suppr[]);

    
#endif